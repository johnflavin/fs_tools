#$Header$
#$Log$

PROG	= Inithdr_test
CSRCS	= ${PROG}.c Inithdr.c endianio.c Getifh.c
FSRCS	=
OBJS	= ${CSRCS:.c=.o} ${FSRCS:.f=.o}

FC	= f77 -O -e -I4
CFLAGS	= -O -I.
LIBS	= -lm

CC	= cc ${CFLAGS}

.c.o:
	${CC} -c $<

.f.o:
	${FC} -c $<

${PROG}: ${OBJS}
	${CC} -o $@ ${OBJS} ${LIBS}

clean:
	/bin/rm ${PROG} ${OBJS}

