#include <string.h>
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include "ANALYZE.h"

#define MAXL		256

typedef struct {
	double r, i;
} CDBLE;

int	write_cdble_as_analyze (CDBLE ***cimage, int ncol, int nrow, int nslice,
		double *dvoxdim, int orient, char *outroot);
void	Inithdr0 (struct dsr *phdr, int *imgdim, float *voxdim, int orient, int minval, int maxval);

void errw (char* program, char* filespc) {
	fprintf (stderr, "%s: %s write error\n", program, filespc);
	exit (-1);
}

int	write_cdble_as_analyze (CDBLE ***cimage, int ncol, int nrow, int nslice,
		double *dvoxdim, int orient, char *outroot)
{
	FILE		*fp;
	struct dsr	hdr;
	char		outfile[MAXL];
	double		q;
	float		voxdim[3];
	int		status, i, minval, maxval, ix, iy, iz, imgdim[4];
	short		norm;

/**********************************************/
/* write norm of complex array as short image */
/**********************************************/
	sprintf (outfile, "%s.img", outroot);
	if (!(fp = fopen (outfile, "wb"))) errw ("write_cdble_as_analyze", outfile);
	maxval = status = 0;
	minval = SHRT_MAX;
	for (iz = 0; iz < nslice; iz++) {
	for (iy = 0; iy < nrow;   iy++) {
	for (ix = 0; ix < ncol;   ix++) {
		q = sqrt (cimage[iz][iy][ix].r*cimage[iz][iy][ix].r + cimage[iz][iy][ix].i*cimage[iz][iy][ix].i);
		if (q > (double) SHRT_MAX) status = -1;
		norm = (short) q + 0.5;
		if (norm < minval) minval = norm;
		if (norm > maxval) maxval = norm;
		fwrite (&norm, sizeof (short), 1, fp);
	}}}
	if (fclose (fp)) errw ("write_cdble_as_analyze", outfile);

/****************************/
/* write analyze 7.5 header */
/****************************/
	for (i = 0; i < 3; i++) voxdim[i] = (float) dvoxdim[i];
	imgdim[0] = ncol;
	imgdim[1] = nrow;
	imgdim[2] = nslice;
	imgdim[3] = 1;
	Inithdr0 (&hdr, imgdim, voxdim, orient, minval, maxval);
	sprintf (outfile, "%s.hdr", outroot);
	if (!(fp = fopen (outfile, "wb")) || fwrite (&hdr, sizeof (struct dsr), 1, fp) != 1
	|| fclose (fp)) errw ("write_cdble_as_analyze", outfile);

	if (status) fprintf (stderr, "write_cdble_as_analyze: one or more %s values out of bounds", outroot);
	return status;
}

void Inithdr0 (struct dsr *phdr, int *imgdim, float *voxdim, int orient, int minval, int maxval)
{
	char		string[MAXL];
	int		j, k;
	int		debug = 0;
	time_t		time_sec;

	memset (phdr, '\0', sizeof (struct dsr));
	phdr->hk.sizeof_hdr = sizeof (struct dsr); 	/* required */
	phdr->hk.extents = 16384;			/* recommended */
	phdr->hk.regular = 'r';				/* required */
	phdr->dime.datatype = 4;			/* short int */
	phdr->dime.bitpix = 16;

	phdr->dime.dim[0] = 4;				/* 4 dimensions always */
	phdr->dime.dim[1] = imgdim[0];
	phdr->dime.dim[2] = imgdim[1];
	phdr->dime.dim[3] = imgdim[2];
	phdr->dime.dim[4] = imgdim[3];
	phdr->dime.pixdim[1] = voxdim[0];
	phdr->dime.pixdim[2] = voxdim[1];
	phdr->dime.pixdim[3] = voxdim[2];

	phdr->hist.orient = orient;
	phdr->dime.glmin = minval;
	phdr->dime.glmax = maxval;

/* start optional history code */
	time (&time_sec);
	strcpy (string, ctime (&time_sec));
	string[24] = '\0';
	if (debug) printf ("%s\n", string);
	for (j = k = 0; k < 10; k++) if (string[k] != ' ') phdr->hist.exp_date[j++] = string[k];
	strncpy (phdr->hist.exp_time,  string + 11, 9);
	strncpy (phdr->hist.generated, string + 20, 9);

	cuserid (string);
	strncpy (phdr->hist.originator, string, 9);
/*   end optional history code */
}
