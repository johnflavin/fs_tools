/*$Header$*/
/*$Log$*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Getifh.h>
#include <endianio.h>

/*************/
/* externals */
/*************/
int Inithdr (struct dsr *phdr, int *imgdim, float *voxdim, char *proto_imgfile);

#define MAXL	256

int main (int argc, char *argv[]) {
	FILE		*fp;
	struct dsr	hdr;
	IFH		ifh;

	char		proto_4dfp[MAXL], proto_hdr[MAXL], out_root[MAXL];
	float		voxdim[3];
	int		imgdim[4], isbig, orient;

	if (argc < 4) {
		fprintf (stderr, "Usage:\tInithdr_test <proto_4dfp> <proto_hdr> <out_root>\n");
		fprintf (stderr, "e.g.,\tInithdr_test VB16168_t2w_atl_333.4dfp VB16168_mpr_n2_atl_711-2V_111.4dfp newfile\n");
		exit (1);
	}
	strcpy (proto_4dfp, argv[1]);
	strcat (proto_4dfp, ".4dfp.img");
	strcpy (proto_hdr, argv[2]);
	strcat (proto_hdr, ".img");
	strcpy (out_root, argv[3]);
	strcat (out_root, ".hdr");

	if (get_4dfp_dimoe (proto_4dfp, imgdim, voxdim, &orient, &isbig)) errr ("Inithdr_test", proto_4dfp);
	Inithdr (&hdr, imgdim, voxdim, proto_hdr);

	if (!(fp = fopen (out_root, "wb")) || fwrite (&hdr, sizeof (struct dsr), 1, fp) != 1
	|| fclose (fp)) errw ("Inithdr_test", out_root);
	printf ("Writing: %s\n", out_root);

	exit (0);
}
