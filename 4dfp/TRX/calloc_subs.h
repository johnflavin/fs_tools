/*$Header: /data/petsun4/data1/src_solaris/TRX/RCS/calloc_subs.h,v 1.2 2008/03/12 03:06:38 avi Exp $*/
/*$Log: calloc_subs.h,v $
 * Revision 1.2  2008/03/12  03:06:38  avi
 * calloc_int2() and free_int2()
 *
 * Revision 1.1  2008/03/11  02:48:02  avi
 * Initial revision
 **/

typedef struct {
	float	r;
	float	i;
} CMPLX;

char	**calloc_char2 (int n1, int n2);
void	free_char2 (char **a);

short	**calloc_short2 (int n1, int n2);
void	free_short2 (short **a);

int	**calloc_int2 (int n1, int n2);
void	free_int2 (int **a);

double	**calloc_double2 (int n1, int n2);
void	free_double2 (double **a);

float	**calloc_float2 (int n1, int n2);
void	free_float2 (float **a);

CMPLX	**calloc_cmplx2 (int n1, int n2);
void	free_cmplx2 (CMPLX **a);

float	***calloc_float3 (int n1, int n2, int n3);
void	free_float3 (float ***a);

double	***calloc_double3 (int n1, int n2, int n3);
void	free_double3 (double ***a);

CMPLX	***calloc_cmplx3 (int n1, int n2, int n3);
void	free_cmplx3 (CMPLX ***a);

float	****calloc_float4 (int n1, int n2, int n3, int n4);
void	free_float4 (float ****a);
