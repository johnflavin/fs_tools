#include <stdio.h>
#include <string.h>
#include <math.h>
#include <limits.h>

void test4 (unsigned char *a) {
	int	i;

	for (i = 0; i < 4; i++) printf (" %2x", a[i]);
	printf ("\n");
}

int CPU_is_bigendian () {
	union {
		float		q;
		unsigned char	c[4];
	} fone;
	fone.q = 1.0;
	return fone.c[0];
}

main (int argc, char *argv[]) {
	int	i, j, k;
	float	q;

	for (j = 0; j < 10; j++) {
		q = j; printf ("%10.4f", q);
		test4 ((unsigned char *) &q);
	}
	printf ("CPU_is_bigendian=%d\n", CPU_is_bigendian ());

	exit (0);
}
