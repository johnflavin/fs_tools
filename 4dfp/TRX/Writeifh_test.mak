#$Header: /data/petsun4/data1/src_solaris/TRX/RCS/Writeifh_test.mak,v 1.1 2006/03/26 23:08:26 avi Exp $
#$Log: Writeifh_test.mak,v $
# Revision 1.1  2006/03/26  23:08:26  avi
# Initial revision
#
PROG	= Writeifh_test
CSRCS	= ${PROG}.c Getifh.c endianio.c
FSRCS	= 
OBJS	= ${CSRCS:.c=.o} ${FSRCS:.f=.o}
LIBS	= -lm

CC	= cc -O -I.
FC	= f77 -O -e -I4

.c.o:
	${CC} -c $<

.f.o:
	${FC} -c $<

${PROG}: ${OBJS}
	${CC} -o $@ ${OBJS} ${LIBS}

clean:
	/bin/rm ${PROG} ${OBJS}

release: ${PROG}
	chmod 755 ${PROG}
	chgrp program ${PROG}
	/bin/mv ${PROG} ${RELEASE}
