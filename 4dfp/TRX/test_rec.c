/*$Header: /data/petsun4/data1/src_solaris/TRX/RCS/test_rec.c,v 1.3 2006/09/23 20:52:02 avi Exp $*/
/*$Log: test_rec.c,v $
 * Revision 1.3  2006/09/23  20:52:02  avi
 * startrecle final argument now control
 *
 * Revision 1.2  2006/09/23  06:32:35  avi
 * test including endian with startrecle()
 *
 * Revision 1.1  2006/03/24  06:47:27  avi
 * Initial revision
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <rec.h>
#include <endianio.h>

#define MAXL	256

void setprog (char *program, char **argv) {
	char *ptr;

	if (!(ptr = strrchr (argv[0], '/'))) ptr = argv[0]; 
	else ptr++;
	strcpy (program, ptr);
}

static char rcsid [] = "$Id: test_rec.c,v 1.3 2006/09/23 20:52:02 avi Exp $";
int main (int argc, char *argv[]) {
	char		control, program[MAXL];

	fprintf (stdout, "%s\n", rcsid);
	setprog (program, argv);

	if (argc < 3) {
		fprintf (stderr, "Usage:\t%s <outfile> <catfile> <random arguments...>\n", program);
		fprintf (stderr, " e.g.:\t%s test_image.4dfp.img existing_image.4dfp.img 123456\n", program);
		exit (-1);
	}

	startrecle (argv[1], argc, argv, rcsid, control);
	printrec ("This is a test\n");
	catrec (argv[2]);
	endrec ();

	exit (0);
}
