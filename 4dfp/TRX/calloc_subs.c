/*$Header: /data/petsun4/data1/src_solaris/TRX/RCS/calloc_subs.c,v 1.3 2009/03/16 02:31:36 avi Exp avi $*/
/*$Log: calloc_subs.c,v $
 * Revision 1.3  2009/03/16  02:31:36  avi
 * correct errc prototype
 *
 * Revision 1.2  2008/03/12  03:06:02  avi
 * calloc_int2() and free_int2()
 *
 * Revision 1.1  2008/03/11  02:48:09  avi
 * Initial revision
 **/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

static void errc (char *program) {
	fprintf (stderr, "%s error\n", program);
	exit (-1);
}

char **calloc_char2 (int n1, int n2) {
	int	i;
	char	**a;

	if (!(a = (char **) malloc (n1 * sizeof (char *)))) errc ("calloc_char2");
	if (!(a[0] = (char *) calloc (n1 * n2, sizeof (char)))) errc ("calloc_char2");
	for (i = 0; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_char2 (char **a) {
	free (a[0]);
	free (a);
}

short **calloc_short2 (int n1, int n2) {
	int	i;
	short	**a;

	if (!(a = (short **) malloc (n1 * sizeof (short *)))) errc ("calloc_short2");
	if (!(a[0] = (short *) calloc (n1 * n2, sizeof (short)))) errc ("calloc_short2");
	for (i = 0; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_short2 (short **a) {
	free (a[0]);
	free (a);
}

#define	USHORT	unsigned short
USHORT **calloc_ushort2 (int n1, int n2) {
	int	i;
	USHORT	**a;

	if (!(a = (USHORT **) malloc (n1 * sizeof (USHORT *)))) errm (program);
	if (!(a[0] = (USHORT *) calloc (n1 * n2, sizeof (USHORT)))) errm (program);
	for (i = 0; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_ushort2 (USHORT **a) {
	free (a[0]);
	free (a);
}

int **calloc_int2 (int n1, int n2) {
	int	i;
	int	**a;

	if (!(a = (int **) malloc (n1 * sizeof (int *)))) errc ("calloc_int2");
	if (!(a[0] = (int *) calloc (n1 * n2, sizeof (int)))) errc ("calloc_int2");
	for (i = 0; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_int2 (int **a) {
	free (a[0]);
	free (a);
}

typedef struct {
	float	r;
	float	i;
} CMPLX;

double **calloc_double2 (int n1, int n2) {
	int	i;
	double	**a;

	if (!(a = (double **) malloc (n1 * sizeof (double *)))) errc ("calloc_double2");
	if (!(a[0] = (double *) calloc (n1 * n2, sizeof (double)))) errc ("calloc_double2");
	for (i = 1; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_double2 (double **a) {
	free (a[0]);
	free (a);
}

CMPLX **calloc_cmplx2 (int n1, int n2) {
	int	i;
	CMPLX	**a;

	if (!(a = (CMPLX **) malloc (n1 * sizeof (CMPLX *)))) errc ("calloc_cmplx2");
	if (!(a[0] = (CMPLX *) calloc (n1 * n2, sizeof (CMPLX)))) errc ("calloc_cmplx2");
	for (i = 1; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_cmplx2 (CMPLX **a) {
	free (a[0]);
	free (a);
}

float **calloc_float2 (int n1, int n2) {
	int	i;
	float	**a;

	if (!(a = (float **) malloc (n1 * sizeof (float *)))) errc ("alloc_float2");
	if (!(a[0] = (float *) calloc (n1 * n2, sizeof (float)))) errc ("alloc_float2");
	for (i = 1; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_float2 (float **a) {
	free (a[0]);
	free (a);
}

float ***calloc_float3 (int n1, int n2, int n3) {
	unsigned int	i, j;
	float		***a;

	if (!(a = (float ***) malloc (n1 * sizeof (float **)))) errc ("calloc_float3");
	if (!(a[0] = (float **) malloc (n1 * n2 * sizeof (float *)))) errc ("calloc_float3");
	if (!(a[0][0] = (float *) calloc (n1 * n2 * n3, sizeof (float)))) errc ("calloc_float3");
	for (i = 0; i < n1; i++) {
		a[i] = a[0] + n2*i;
		for (j = 0; j < n2; j++) {
			a[i][j] = a[0][0] + n3*(n2*i + j);
		}
	}
	return a;
}

void free_float3 (float ***a) {
	free (a[0][0]);
	free (a[0]);
	free (a);
}

CMPLX ***calloc_cmplx3 (int n1, int n2, int n3) {
	unsigned int	i, j;
	CMPLX		***a;

	if (!(a = (CMPLX ***) malloc (n1 * sizeof (CMPLX **)))) errc ("calloc_cmplx3");
	if (!(a[0] = (CMPLX **) malloc (n1 * n2 * sizeof (CMPLX *)))) errc ("calloc_cmplx3");
	if (!(a[0][0] = (CMPLX *) calloc (n1 * n2 * n3, sizeof (CMPLX)))) errc ("calloc_cmplx3");
	for (i = 0; i < n1; i++) {
		a[i] = a[0] + n2*i;
		for (j = 0; j < n2; j++) {
			a[i][j] = a[0][0] + n3*(n2*i + j);
		}
	}
	return a;
}

void free_cmplx3 (CMPLX ***a) {
	free (a[0][0]);
	free (a[0]);
	free (a);
}

double ***calloc_double3 (int n1, int n2, int n3) {
	unsigned int	i, j;
	double		***a;

	if (!(a = (double ***) malloc (n1 * sizeof (double **)))) errc ("calloc_double3");
	if (!(a[0] = (double **) malloc (n1 * n2 * sizeof (double *)))) errc ("calloc_double3");
	if (!(a[0][0] = (double *) calloc (n1 * n2 * n3, sizeof (double)))) errc ("calloc_double3");
	for (i = 0; i < n1; i++) {
		a[i] = a[0] + n2*i;
		for (j = 0; j < n2; j++) {
			a[i][j] = a[0][0] + n3*(n2*i + j);
		}
	}
	return a;
}

void free_double3 (double ***a) {
	free (a[0][0]);
	free (a[0]);
	free (a);
}

float ****calloc_float4 (int n1, int n2, int n3, int n4) {
	unsigned int	i, j, k, ii, jj, kk;
	float		****a;

	if (!(a = (float ****) malloc (n1 * sizeof (float ***)))) errc ("calloc_float4");
	if (!(a[0] = (float ***) malloc (n1 * n2 * sizeof (float **)))) errc ("calloc_float4");
	if (!(a[0][0] = (float **) malloc (n1 * n2 * n3 * sizeof (float *)))) errc ("calloc_float4");
	if (!(a[0][0][0] = (float *) calloc (n1 * n2 * n3 * n4, sizeof (float)))) errc ("calloc_float4");
	for (i = 0; i < n1; i++) {
		ii = n2*i;		/* ii += n2 fails with optimizing compiler */
		a[i] = a[0] + ii;
		for (j = 0; j < n2; j++) {
			jj = n3*(ii + j);
			a[i][j] = a[0][0] + jj;
			for (k = 0; k < n3; k++) {
				kk = n4*(jj + k);
				a[i][j][k] = a[0][0][0] + kk;
			}
		}
	}
	return a;
}

void free_float4 (float ****a) {
	free (a[0][0][0]);
	free (a[0][0]);
	free (a[0]);
	free (a);
}
