      subroutine spline3d_test
      parameter (nx=15)
      parameter (ny=9)
      parameter (nz=5)
      real*4 f(0:nx-1,0:ny-1,0:nz-1)
      real*4 d2zf(0:nx-1,0:ny-1,0:nz-1)
      real*4 d2xf(0:nx-1,0:ny-1,0:nz-1)
      real*4 d2yf(0:nx-1,0:ny-1,0:nz-1)
      real*4 g(-nx/2:nx/2,-ny/2:ny/2,-nz/2:nz/2)
      real*4 d(-nx/2:nx/2)
      real*4 splin3d(1)
      data del/0.2/

      ix0=nx/2
      iy0=ny/2
      iz0=nz/2
      do 2 iz=0,nz-1
      do 2 iy=0,ny-1
      do 2 ix=0,nx-1
      f(ix,iy,iz)=0.
c     if(mod(ix+iy+iz,7).eq.0)f(ix,iy,iz)=1.
    2 continue
      f(ix0,iy0,iz0)=1.

      call spline3d(f,nx,ny,nz,d2xf,d2yf,d2zf)

      do 3 iz=-nz/2,nz/2
      do 3 iy=-ny/2,ny/2
      do 3 ix=-nx/2,nx/2
      z=float(iz0)+del*float(iz)
      y=float(iy0)+del*float(iy)
      x=float(ix0)+del*float(ix)
      call splint3d(f,nx,ny,nz,d2xf,d2yf,d2zf,x,y,z,splin3d)
      g(ix,iy,iz)=splin3d(1)
    3 continue

      do 4 ix=-nx/2,nx/2
    4 d(ix)=float(ix0)+del*float(ix)

      type "('g')"
      do 51 iz=-nz/2,nz/2
      type "('iz=',i2,' z=',f5.2)",iz,float(iz0)+del*float(iz)
      type "(6x,17f5.2)",d
      type "(6x,17('-----'))"
      do iy=-ny/2,ny/2
        type "(f5.2,'|',17f5.2)",float(iy0)+del*float(iy),(g(ix,iy,iz),ix=-nx/2,nx/2)
      enddo
   51 continue

      type "('d2xf')"
      do 21 iz=0,nz-1
      type "('iz=',i1)",iz
      do iy=0,ny-1
        type "(i3,17f5.2)",iy,(d2xf(ix,iy,iz),ix=0,nx-1)
      enddo
   21 continue

      type "('d2yf')"
      do 31 iz=0,nz-1
      type "('iz=',i1)",iz
      do iy=0,ny-1
        type "(i3,17f5.2)",iy,(d2yf(ix,iy,iz),ix=0,nx-1)
      enddo
   31 continue

      type "('d2zf')"
      do 41 iz=0,nz-1
      type "('iz=',i1)",iz
      do iy=0,ny-1
        type "(i3,17f5.2)",iy,(d2zf(ix,iy,iz),ix=0,nx-1)
      enddo
   41 continue

      call exit(0)
      end

      subroutine spline3d(imag,nx,ny,nz,d2xi,d2yi,d2zi)
      real*4 imag(0:nx-1,0:ny-1,0:nz-1),d2xi(0:nx-1,0:ny-1,0:nz-1)
      real*4 d2yi(0:nx-1,0:ny-1,0:nz-1),d2zi(0:nx-1,0:ny-1,0:nz-1)
      real*4 ax(0:nx-1,0:nx-1)
      real*4 ay(0:ny-1,0:ny-1)
      real*4 az(0:nz-1,0:nz-1)
      real*4 u(0:2047)
      pointer (pax,ax),(pay,ay),(paz,az),(pu,u)

      n=max0(nx,ny,nz)
      pu=malloc(4*n)

      paz=malloc(4*nz**2)
      do 13 iz=0,nz-1
      do 13 jz=0,nz-1
   13 az(iz,jz)=0.
      az(0,0)=1.
      az(nz-1,nz-1)=1.
      do 12 iz=1,nz-2
      az(iz,iz-1)=1./6.
      az(iz,iz+0)=2./3.
   12 az(iz,iz+1)=1./6.
      call matinv(az,nz,det)
      do 11 ix=0,nx-1
      do 11 iy=0,ny-1
      u(0)=0.
      do iz=1,nz-2
        u(iz)=imag(ix,iy,iz+1)-2.*imag(ix,iy,iz)+imag(ix,iy,iz-1)
      enddo
      u(nz-1)=0.
      do iz=0,nz-1
        d2zi(ix,iy,iz)=0.
        do jz=0,nz-1
          d2zi(ix,iy,iz)=d2zi(ix,iy,iz)+az(iz,jz)*u(jz)
        enddo
      enddo
   11 continue
      call free(paz)

      pay=malloc(4*ny**2)
      do 23 iy=0,ny-1
      do 23 jy=0,ny-1
   23 ay(iy,jy)=0.
      ay(0,0)=1.
      ay(ny-1,ny-1)=1.
      do 22 iy=1,ny-2
      ay(iy,iy-1)=1./6.
      ay(iy,iy+0)=2./3.
   22 ay(iy,iy+1)=1./6.
      call matinv(ay,ny,det)
      do 21 ix=0,nx-1
      do 21 iz=0,nz-1
      u(0)=0.
      do iy=1,ny-2
        u(iy)=imag(ix,iy+1,iz)-2.*imag(ix,iy,iz)+imag(ix,iy-1,iz)
      enddo
      u(ny-1)=0.
      do iy=0,ny-1
        d2yi(ix,iy,iz)=0.
        do jy=0,ny-1
          d2yi(ix,iy,iz)=d2yi(ix,iy,iz)+ay(iy,jy)*u(jy)
        enddo
      enddo
   21 continue
      call free(pay)

      pax=malloc(4*nx**2)
      do 33 ix=0,nx-1
      do 33 jx=0,nx-1
   33 ax(ix,jx)=0.
      ax(0,0)=1.
      ax(nx-1,nx-1)=1.
      do 32 ix=1,nx-2
      ax(ix,ix-1)=1./6.
      ax(ix,ix+0)=2./3.
   32 ax(ix,ix+1)=1./6.
      call matinv(ax,nx,det)
      do 31 iy=0,ny-1
      do 31 iz=0,nz-1
      u(0)=0.
      do ix=1,nx-2
        u(ix)=imag(ix+1,iy,iz)-2.*imag(ix,iy,iz)+imag(ix-1,iy,iz)
      enddo
      u(nx-1)=0.
      do ix=0,nx-1
        d2xi(ix,iy,iz)=0.
        do jx=0,nx-1
          d2xi(ix,iy,iz)=d2xi(ix,iy,iz)+ax(ix,jx)*u(jx)
        enddo
      enddo
   31 continue
      call free(pax)

      call free(pu)
      return
      end

      subroutine splint3d(imag,nx,ny,nz,d2xi,d2yi,d2zi,x,y,z,splin3d)
      real*4 imag(0:nx-1,0:ny-1,0:nz-1),d2xi(0:nx-1,0:ny-1,0:nz-1)
      real*4 d2yi(0:nx-1,0:ny-1,0:nz-1),d2zi(0:nx-1,0:ny-1,0:nz-1)
      real*4 splin3d(1)
      static interp,nx0,ny0,nz0
      integer*4 nx0/0/
      integer*4 ny0/0/
      integer*4 nz0/0/

      ix=nint(x-.5)
      if(nx.eq.1.or.ix.lt.0)then
        ix=0
        ax=1.
        wx=0.
        cx=0.
        dx=0.
      elseif(ix.ge.nx-1)then
        ix=nx-2
        ax=0.
        wx=1.
        cx=0.
        dx=0.
      else
        wx=x-float(ix)
        ax=1.-wx
        cx=ax*(ax*ax-1.)/6.
        dx=wx*(wx*wx-1.)/6.
      endif

      iy=nint(y-.5)
      if(ny.eq.1.or.iy.lt.0)then
        iy=0
        ay=1.
        wy=0.
        cy=0.
        dy=0.
      elseif(iy.ge.ny-1)then
        iy=ny-2
        ay=0.
        wy=1.
        cy=0.
        dy=0.
      else
        wy=y-float(iy)
        ay=1.-wy
        cy=ay*(ay*ay-1.)/6.
        dy=wy*(wy*wy-1.)/6.
      endif

      iz=nint(z-.5)
      if(nz.eq.1.or.iz.lt.0)then
        iz=0
        az=1.
        wz=0.
        cz=0.
        dz=0.
      elseif(iz.ge.nz-1)then
        iz=nz-2
        az=0.
        wz=1.
        cz=0.
        dz=0.
      else
        wz=z-float(iz)
        az=1.-wz
        cz=az*(az*az-1.)/6.
        dz=wz*(wz*wz-1.)/6.
      endif

      if(nx.ne.nx0.or.ny.ne.ny0.or.nz.ne.nz0)then
        if(nx.ge.3.and.ny.ge.3.and.nz.ge.3)assign 333 to interp
        if(nx.ge.3.and.ny.ge.3.and.nz.eq.2)assign 332 to interp
        if(nx.ge.3.and.ny.ge.3.and.nz.eq.1)assign 331 to interp
        if(nx.ge.3.and.ny.eq.2.and.nz.ge.3)assign 323 to interp
        if(nx.ge.3.and.ny.eq.2.and.nz.eq.2)assign 322 to interp
        if(nx.ge.3.and.ny.eq.2.and.nz.eq.1)assign 321 to interp
        if(nx.ge.3.and.ny.eq.1.and.nz.ge.3)assign 313 to interp
        if(nx.ge.3.and.ny.eq.1.and.nz.eq.2)assign 312 to interp
        if(nx.ge.3.and.ny.eq.1.and.nz.eq.1)assign 311 to interp
        if(nx.eq.2.and.ny.ge.3.and.nz.ge.3)assign 233 to interp
        if(nx.eq.2.and.ny.ge.3.and.nz.eq.2)assign 232 to interp
        if(nx.eq.2.and.ny.ge.3.and.nz.eq.1)assign 231 to interp
        if(nx.eq.2.and.ny.eq.2.and.nz.ge.3)assign 223 to interp
        if(nx.eq.2.and.ny.eq.2.and.nz.eq.2)assign 222 to interp
        if(nx.eq.2.and.ny.eq.2.and.nz.eq.1)assign 221 to interp
        if(nx.eq.2.and.ny.eq.1.and.nz.ge.3)assign 213 to interp
        if(nx.eq.2.and.ny.eq.1.and.nz.eq.2)assign 212 to interp
        if(nx.eq.2.and.ny.eq.1.and.nz.eq.1)assign 211 to interp
        if(nx.eq.1.and.ny.ge.3.and.nz.ge.3)assign 133 to interp
        if(nx.eq.1.and.ny.ge.3.and.nz.eq.2)assign 132 to interp
        if(nx.eq.1.and.ny.ge.3.and.nz.eq.1)assign 131 to interp
        if(nx.eq.1.and.ny.eq.2.and.nz.ge.3)assign 123 to interp
        if(nx.eq.1.and.ny.eq.2.and.nz.eq.2)assign 122 to interp
        if(nx.eq.1.and.ny.eq.2.and.nz.eq.1)assign 121 to interp
        if(nx.eq.1.and.ny.eq.1.and.nz.ge.3)assign 113 to interp
        if(nx.eq.1.and.ny.eq.1.and.nz.eq.2)assign 112 to interp
        if(nx.eq.1.and.ny.eq.1.and.nz.eq.1)assign 111 to interp
        nx0=nx
        ny0=ny
        nz0=nz
      endif
      goto interp

  333 splin3d(1)
     &=ax*(ay*(az*imag(ix  ,iy,iz)+wz*imag(ix  ,iy,iz+1))+wy*(az*imag(ix  ,iy+1,iz)+wz*imag(ix  ,iy+1,iz+1)))
     &+wx*(ay*(az*imag(ix+1,iy,iz)+wz*imag(ix+1,iy,iz+1))+wy*(az*imag(ix+1,iy+1,iz)+wz*imag(ix+1,iy+1,iz+1)))
     &+cx*(ay*(az*d2xi(ix  ,iy,iz)+wz*d2xi(ix  ,iy,iz+1))+wy*(az*d2xi(ix  ,iy+1,iz)+wz*d2xi(ix  ,iy+1,iz+1)))
     &+dx*(ay*(az*d2xi(ix+1,iy,iz)+wz*d2xi(ix+1,iy,iz+1))+wy*(az*d2xi(ix+1,iy+1,iz)+wz*d2xi(ix+1,iy+1,iz+1)))
     &+ax*(cy*(az*d2yi(ix  ,iy,iz)+wz*d2yi(ix  ,iy,iz+1))+dy*(az*d2yi(ix  ,iy+1,iz)+wz*d2yi(ix  ,iy+1,iz+1)))
     &+wx*(cy*(az*d2yi(ix+1,iy,iz)+wz*d2yi(ix+1,iy,iz+1))+dy*(az*d2yi(ix+1,iy+1,iz)+wz*d2yi(ix+1,iy+1,iz+1)))
     &+ax*(ay*(cz*d2zi(ix  ,iy,iz)+dz*d2zi(ix  ,iy,iz+1))+wy*(cz*d2zi(ix  ,iy+1,iz)+dz*d2zi(ix  ,iy+1,iz+1)))
     &+wx*(ay*(cz*d2zi(ix+1,iy,iz)+dz*d2zi(ix+1,iy,iz+1))+wy*(cz*d2zi(ix+1,iy+1,iz)+dz*d2zi(ix+1,iy+1,iz+1)))
      return
  233 splin3d(1)
     &=ax*(ay*(az*imag(0   ,iy,iz)+wz*imag(0   ,iy,iz+1))+wy*(az*imag(0   ,iy+1,iz)+wz*imag(0   ,iy+1,iz+1)))
     &+wx*(ay*(az*imag(1   ,iy,iz)+wz*imag(1   ,iy,iz+1))+wy*(az*imag(1   ,iy+1,iz)+wz*imag(1   ,iy+1,iz+1)))
     &+ax*(cy*(az*d2yi(0   ,iy,iz)+wz*d2yi(0   ,iy,iz+1))+dy*(az*d2yi(0   ,iy+1,iz)+wz*d2yi(0   ,iy+1,iz+1)))
     &+wx*(cy*(az*d2yi(1   ,iy,iz)+wz*d2yi(1   ,iy,iz+1))+dy*(az*d2yi(1   ,iy+1,iz)+wz*d2yi(1   ,iy+1,iz+1)))
     &+ax*(ay*(cz*d2zi(0   ,iy,iz)+dz*d2zi(0   ,iy,iz+1))+wy*(cz*d2zi(0   ,iy+1,iz)+dz*d2zi(0   ,iy+1,iz+1)))
     &+wx*(ay*(cz*d2zi(1   ,iy,iz)+dz*d2zi(1   ,iy,iz+1))+wy*(cz*d2zi(1   ,iy+1,iz)+dz*d2zi(1   ,iy+1,iz+1)))
      return
  323 splin3d(1)
     &=ax*(ay*(az*imag(ix  ,0 ,iz)+wz*imag(ix  ,0 ,iz+1))+wy*(az*imag(ix  ,1   ,iz)+wz*imag(ix  ,1   ,iz+1)))
     &+wx*(ay*(az*imag(ix+1,0 ,iz)+wz*imag(ix+1,0 ,iz+1))+wy*(az*imag(ix+1,1   ,iz)+wz*imag(ix+1,1   ,iz+1)))
     &+cx*(ay*(az*d2xi(ix  ,0 ,iz)+wz*d2xi(ix  ,0 ,iz+1))+wy*(az*d2xi(ix  ,1   ,iz)+wz*d2xi(ix  ,1   ,iz+1)))
     &+dx*(ay*(az*d2xi(ix+1,0 ,iz)+wz*d2xi(ix+1,0 ,iz+1))+wy*(az*d2xi(ix+1,1   ,iz)+wz*d2xi(ix+1,1   ,iz+1)))
     &+ax*(ay*(cz*d2zi(ix  ,0 ,iz)+dz*d2zi(ix  ,0 ,iz+1))+wy*(cz*d2zi(ix  ,1   ,iz)+dz*d2zi(ix  ,1   ,iz+1)))
     &+wx*(ay*(cz*d2zi(ix+1,0 ,iz)+dz*d2zi(ix+1,0 ,iz+1))+wy*(cz*d2zi(ix+1,1   ,iz)+dz*d2zi(ix+1,1   ,iz+1)))
      return
  332 splin3d(1)
     &=ax*(ay*(az*imag(ix  ,iy,0 )+wz*imag(ix  ,iy,1   ))+wy*(az*imag(ix  ,iy+1,0 )+wz*imag(ix  ,iy+1,1   )))
     &+wx*(ay*(az*imag(ix+1,iy,0 )+wz*imag(ix+1,iy,1   ))+wy*(az*imag(ix+1,iy+1,0 )+wz*imag(ix+1,iy+1,1   )))
     &+cx*(ay*(az*d2xi(ix  ,iy,0 )+wz*d2xi(ix  ,iy,1   ))+wy*(az*d2xi(ix  ,iy+1,0 )+wz*d2xi(ix  ,iy+1,1   )))
     &+dx*(ay*(az*d2xi(ix+1,iy,0 )+wz*d2xi(ix+1,iy,1   ))+wy*(az*d2xi(ix+1,iy+1,0 )+wz*d2xi(ix+1,iy+1,1   )))
     &+ax*(cy*(az*d2yi(ix  ,iy,0 )+wz*d2yi(ix  ,iy,1   ))+dy*(az*d2yi(ix  ,iy+1,0 )+wz*d2yi(ix  ,iy+1,1   )))
     &+wx*(cy*(az*d2yi(ix+1,iy,0 )+wz*d2yi(ix+1,iy,1   ))+dy*(az*d2yi(ix+1,iy+1,0 )+wz*d2yi(ix+1,iy+1,1   )))
      return
  322 splin3d(1)
     &=ax*(ay*(az*imag(ix  ,0 ,0 )+wz*imag(ix  ,0 ,1   ))+wy*(az*imag(ix  ,1   ,0 )+wz*imag(ix  ,1   ,1   )))
     &+wx*(ay*(az*imag(ix+1,0 ,0 )+wz*imag(ix+1,0 ,1   ))+wy*(az*imag(ix+1,1   ,0 )+wz*imag(ix+1,1   ,1   )))
     &+cx*(ay*(az*d2xi(ix  ,0 ,0 )+wz*d2xi(ix  ,0 ,1   ))+wy*(az*d2xi(ix  ,1   ,0 )+wz*d2xi(ix  ,1   ,1   )))
     &+dx*(ay*(az*d2xi(ix+1,0 ,0 )+wz*d2xi(ix+1,0 ,1   ))+wy*(az*d2xi(ix+1,1   ,0 )+wz*d2xi(ix+1,1   ,1   )))
      return
  232 splin3d(1)
     &=ax*(ay*(az*imag(0   ,iy,0 )+wz*imag(0   ,iy,1   ))+wy*(az*imag(0   ,iy+1,0 )+wz*imag(0   ,iy+1,1   )))
     &+wx*(ay*(az*imag(1   ,iy,0 )+wz*imag(1   ,iy,1   ))+wy*(az*imag(1   ,iy+1,0 )+wz*imag(1   ,iy+1,1   )))
     &+ax*(cy*(az*d2yi(0   ,iy,0 )+wz*d2yi(0   ,iy,1   ))+dy*(az*d2yi(0   ,iy+1,0 )+wz*d2yi(0   ,iy+1,1   )))
     &+wx*(cy*(az*d2yi(1   ,iy,0 )+wz*d2yi(1   ,iy,1   ))+dy*(az*d2yi(1   ,iy+1,0 )+wz*d2yi(1   ,iy+1,1   )))
      return
  223 splin3d(1)
     &=ax*(ay*(az*imag(0   ,0 ,iz)+wz*imag(0   ,0 ,iz+1))+wy*(az*imag(0   ,1   ,iz)+wz*imag(0   ,1   ,iz+1)))
     &+wx*(ay*(az*imag(1   ,0 ,iz)+wz*imag(1   ,0 ,iz+1))+wy*(az*imag(1   ,1   ,iz)+wz*imag(1   ,1   ,iz+1)))
     &+ax*(ay*(cz*d2zi(0   ,0 ,iz)+dz*d2zi(0   ,0 ,iz+1))+wy*(cz*d2zi(0   ,1   ,iz)+dz*d2zi(0   ,1   ,iz+1)))
     &+wx*(ay*(cz*d2zi(1   ,0 ,iz)+dz*d2zi(1   ,0 ,iz+1))+wy*(cz*d2zi(1   ,1   ,iz)+dz*d2zi(1   ,1   ,iz+1)))
      return
  222 splin3d(1)
     &=ax*(ay*(az*imag(0   ,0 ,0 )+wz*imag(0   ,0 ,1   ))+wy*(az*imag(0   ,1   ,0 )+wz*imag(0   ,1   ,1   )))
     &+wx*(ay*(az*imag(1   ,0 ,0 )+wz*imag(1   ,0 ,1   ))+wy*(az*imag(1   ,1   ,0 )+wz*imag(1   ,1   ,1   )))
      return
  133 splin3d(1)
     &=ay*(az*imag(0,iy  ,iz)+wz*imag(0,iy  ,iz+1))
     &+wy*(az*imag(0,iy+1,iz)+wz*imag(0,iy+1,iz+1))
     &+cy*(az*d2yi(0,iy  ,iz)+wz*d2yi(0,iy  ,iz+1))
     &+dy*(az*d2yi(0,iy+1,iz)+wz*d2yi(0,iy+1,iz+1))
     &+ay*(cz*d2zi(0,iy  ,iz)+dz*d2zi(0,iy  ,iz+1))
     &+wy*(cz*d2zi(0,iy+1,iz)+dz*d2zi(0,iy+1,iz+1))
      return
  123 splin3d(1)
     &=ay*(az*imag(0,0   ,iz)+wz*imag(0,0   ,iz+1))
     &+wy*(az*imag(0,1   ,iz)+wz*imag(0,1   ,iz+1))
     &+ay*(cz*d2zi(0,0   ,iz)+dz*d2zi(0,0   ,iz+1))
     &+wy*(cz*d2zi(0,1   ,iz)+dz*d2zi(0,1   ,iz+1))
      return
  132 splin3d(1)
     &=ay*(az*imag(0,iy  ,0 )+wz*imag(0,iy  ,1   ))
     &+wy*(az*imag(0,iy+1,0 )+wz*imag(0,iy+1,1   ))
     &+cy*(az*d2yi(0,iy  ,0 )+wz*d2yi(0,iy  ,1   ))
     &+dy*(az*d2yi(0,iy+1,0 )+wz*d2yi(0,iy+1,1   ))
      return
  122 splin3d(1)
     &=ay*(az*imag(0,0   ,0 )+wz*imag(0,0   ,1   ))
     &+wy*(az*imag(0,1   ,0 )+wz*imag(0,1   ,1   ))
      return
  313 splin3d(1)
     &=ax*(az*imag(ix  ,0,iz)+wz*imag(ix  ,0,iz+1))
     &+wx*(az*imag(ix+1,0,iz)+wz*imag(ix+1,0,iz+1))
     &+cx*(az*d2xi(ix  ,0,iz)+wz*d2xi(ix  ,0,iz+1))
     &+dx*(az*d2xi(ix+1,0,iz)+wz*d2xi(ix+1,0,iz+1))
     &+ax*(cz*d2zi(ix  ,0,iz)+dz*d2zi(ix  ,0,iz+1))
     &+wx*(cz*d2zi(ix+1,0,iz)+dz*d2zi(ix+1,0,iz+1))
      return
  213 splin3d(1)
     &=ax*(az*imag(0   ,0,iz)+wz*imag(0   ,0,iz+1))
     &+wx*(az*imag(1   ,0,iz)+wz*imag(1   ,0,iz+1))
     &+ax*(cz*d2zi(0   ,0,iz)+dz*d2zi(0   ,0,iz+1))
     &+wx*(cz*d2zi(1   ,0,iz)+dz*d2zi(1   ,0,iz+1))
      return
  312 splin3d(1)
     &=ax*(az*imag(ix  ,0,0 )+wz*imag(ix  ,0,1   ))
     &+wx*(az*imag(ix+1,0,0 )+wz*imag(ix+1,0,1   ))
     &+cx*(az*d2xi(ix  ,0,0 )+wz*d2xi(ix  ,0,1   ))
     &+dx*(az*d2xi(ix+1,0,0 )+wz*d2xi(ix+1,0,1   ))
      return
  212 splin3d(1)
     &=ax*(az*imag(0   ,0,0 )+wz*imag(0   ,0,1   ))
     &+wx*(az*imag(1   ,0,0 )+wz*imag(1   ,0,1   ))
      return
  331 splin3d(1)
     &=ax*(ay*imag(ix  ,iy,0)+wy*imag(ix  ,iy+1,0))
     &+wx*(ay*imag(ix+1,iy,0)+wy*imag(ix+1,iy+1,0))
     &+cx*(ay*d2xi(ix  ,iy,0)+wy*d2xi(ix  ,iy+1,0))
     &+dx*(ay*d2xi(ix+1,iy,0)+wy*d2xi(ix+1,iy+1,0))
     &+ax*(cy*d2yi(ix  ,iy,0)+dy*d2yi(ix  ,iy+1,0))
     &+wx*(cy*d2yi(ix+1,iy,0)+dy*d2yi(ix+1,iy+1,0))
      return
  231 splin3d(1)
     &=ax*(ay*imag(0   ,iy,0)+wy*imag(0   ,iy+1,0))
     &+wx*(ay*imag(1   ,iy,0)+wy*imag(1   ,iy+1,0))
     &+ax*(cy*d2yi(0   ,iy,0)+dy*d2yi(0   ,iy+1,0))
     &+wx*(cy*d2yi(1   ,iy,0)+dy*d2yi(1   ,iy+1,0))
      return
  321 splin3d(1)
     &=ax*(ay*imag(ix  ,0 ,0)+wy*imag(ix  ,1   ,0))
     &+wx*(ay*imag(ix+1,0 ,0)+wy*imag(ix+1,1   ,0))
     &+cx*(ay*d2xi(ix  ,0 ,0)+wy*d2xi(ix  ,1   ,0))
     &+dx*(ay*d2xi(ix+1,0 ,0)+wy*d2xi(ix+1,1   ,0))
      return
  221 splin3d(1)
     &=ax*(ay*imag(0   ,0 ,0)+wy*imag(0   ,1   ,0))
     &+wx*(ay*imag(1   ,0 ,0)+wy*imag(1   ,1   ,0))
      return
  113 splin3d(1)=az*imag(0,0,iz)+wz*imag(0,0,iz+1)+cz*d2zi(0,0,iz)+dz*d2zi(0,0,iz+1)
      return
  131 splin3d(1)=ay*imag(0,iy,0)+wy*imag(0,iy+1,0)+cy*d2yi(0,iy,0)+dy*d2yi(0,iy+1,0)
      return
  311 splin3d(1)=ax*imag(ix,0,0)+wx*imag(ix+1,0,0)+cx*d2xi(ix,0,0)+dx*d2xi(ix+1,0,0)
      return
  112 splin3d(1)=az*imag(0,0,0)+wz*imag(0,0,1)
      return
  121 splin3d(1)=ay*imag(0,0,0)+wy*imag(0,1,0)
      return
  211 splin3d(1)=ax*imag(0,0,0)+wx*imag(1,0,0)
      return
  111 splin3d(1)=imag(0,0,0)
      return
      end
