/*$Id: algebra_4dfp.h,v 1.1 1998/03/29 07:01:33 tconturo Exp $*/
/*$Log: algebra_4dfp.h,v $
 * Revision 1.1  1998/03/29  07:01:33  tconturo
 * Initial revision
 *
 * Revision 1.1  1998/03/29  07:00:15  tconturo
 * Initial revision
 *
 * Revision 1.1  1998/03/29  05:57:43  tconturo
 * Initial revision
 *
 * Revision 1.2  1996/05/19  21:07:09  ty7777
 * Before making further changes.
 *
 * Revision 1.1  1996/05/17  16:13:25  ty7777
 * Initial revision
 **/

/*____________________________________________________________________________	
	File:		algebra_4dfp.h

	Description:	Header file for algebra_4dfp.

	Author:		Tom Yang, 02/21/94
_____________________________________________________________________________*/	

#ifndef _ALGEBRA_4DFP_INCLUDED
#define _ALGEBRA_4DFP_INCLUDED

#include <petutil/petutil.h>

static char rcsid_algebra_4dfp [] = "$Header: /data/petsun4/data1/src_solaris/algebra_4dfp/RCS/algebra_4dfp.h,v 1.1 1998/03/29 07:01:33 tconturo Exp $";

#define	RECTANGLE		1
#define	ELLIPSE			2
#define MAX_LST4DFP_SIZE	512

typedef struct {
	int	rgn_shape;		/* RECTANGLE, ELLIPSE */
	int	rotation_angle;		/* in degrees */		
	POINT3D	center;			/* center of voi, in pixels */
	POINT3D	dimension;		/* x, y, z dimension of voi, in pixels */
} RGNDATA;				/* region structure */ 

PUBLIC	int	readrgn ();
PUBLIC	int	average ();

#endif /* _ALGEBRA_4DFP_INCLUDED */

