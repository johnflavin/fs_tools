/*$Header$*/
/*$Log$*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <unistd.h>			/* getpid () */
#include <Getifh.h>
#include <endianio.h>
#include <rec.h>
#include <conc.h>			/* defines MAXL */

#define MAXC		8192		/* maximum number of input profle columns */	
#define MAX(a,b)	(a>b? a:b)
#define MIN(a,b)	(a<b? a:b)

int	expandf (char *string, int len);							/* expandf.c */
float	fimg_mode (float* fimg, int nval);							/* fimg_mode.c */

/**************/
/* dsvdcmp0.c */
/**************/
extern void	 dsvdcmp0  (double **a, int m, int n, double *w, double **v);			/* dsvdcmp0.c */
extern int	ndsvdcmp0  (double **a, int m, int n, double *w, double **v, double tol);

/********************/
/* global variables */
/********************/
static char	rcsid[] = "$Id: glm_4dfp.c,v 1.22 2011/02/07 04:47:40 avi Exp avi $";
static char	program[MAXL];
static int	debug = 0;

double **calloc_double2 (int n1, int n2) {
	int	i;
	double	**a;

	if (!(a = (double **) malloc (n1 * sizeof (double *)))) errm (program);
	if (!(a[0] = (double *) calloc (n1 * n2, sizeof (double)))) errm (program);
	for (i = 1; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_double2 (double **a) {
	free (a[0]);
	free (a);
}

float **calloc_float2 (int n1, int n2) {
	int	i;
	float	**a;

	if (!(a = (float **) malloc (n1 * sizeof (float *)))) errm (program);
	if (!(a[0] = (float *) calloc (n1 * n2, sizeof (float)))) errm (program);
	for (i = 1; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_float2 (float **a) {
	free (a[0]);
	free (a);
}

double dzeromean (double *f, int npts) {
	int		i;
	double		u;

	for (u = i = 0; i < npts; i++) u += f[i];
	u /= npts;
	for (i = 0; i < npts; i++) f[i] -= u;
	if (debug) for (i = 0; i < npts; i++) {
		printf ("%4d %10.6f\n", i + 1, f[i]);
	}
	return u;
}

double dunitvar (double *f, int npts) {
	int		i;
	double		v;

	for (v = i = 0; i < npts; i++) v += f[i]*f[i];
	v /= npts;
	for (i = 0; i < npts; i++) f[i] /= sqrt (v);
	if (debug) for (i = 0; i < npts; i++) {
		printf ("%4d %10.6f\n", i + 1, f[i]);
	}
	return v;
}

void dmatlst (double **a, int n) {
	int	i, j;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) printf ("%11.7f", a[j][i]);
		printf ("\n");
	}
}

void usage (char *program) {
	fprintf (stderr, "Usage:\t%s <format> <4dfp|conc input>\n", program);
	fprintf (stderr, "e.g.,\t%s \"4x124+\" b1_rmsp_dbnd_xr3d_norm\n", program);
	fprintf (stderr, "\toption\n");
	fprintf (stderr, "\t-@<b|l>\toutput big or little endian (default input endian)\n");
	fprintf (stderr, "N.B.:\tconc files must have extension \"conc\"\n");
	exit (1);
}

int main (int argc, char *argv[]) {
/**********************/
/* filename variables */
/**********************/
	FILE		*tmpfp, *imgfp, *coefp, *outfp;
	char		coeroot[MAXL], coefile[MAXL];	/* coefficients image */
	char		imgroot[MAXL], imgfile[MAXL];	/* input 4dfp stack filename */
	char		outroot[MAXL], outfile[MAXL], tmpfile[MAXL], profile[MAXL];

/**********************/
/* image dimensioning */
/**********************/
	CONC_BLOCK	conc_block;		/* conc i/o control block */
	IFH		ifhimg, ifhcoe;
	float		*imgt;			/* one volume */
	float		*imga;			/* voxelwise mean */
	float		*imgv;			/* voxelwise var */
	int		*imgm;			/* mask of voxels defined at all time points */
	float		voxdim[3], mmppix[3], center[3];
	int		jndex, imgdim[4], vdim;	/* image dimensions */
	int		isbig, isbigr;
	char		control = '\0';

/*************************/
/* timeseries processing */
/*************************/
	char		*format;
	float		fmin, fmax, fmode;
	double		errmax;
	double		tol = 1.e-4, **imgb, **imge;
	double		*lambda, **v;
	int		npts, nnez, nred;

/***********/
/* utility */
/***********/
	char		command[MAXL], *ptr;
	int		c, i, j, k, m;
	double		q;

/*********/
/* flags */
/*********/
	int		conc_flag = 0;
	int		status = 0;

	printf ("%s\n", rcsid);
	if (!(ptr = strrchr (argv[0], '/'))) ptr = argv[0]; else ptr++;
	strcpy (program, ptr);

	if (!(format = (char *) calloc (1024, sizeof (char)))) errm (program);
/************************/
/* process command line */
/************************/
	for (k = 0, i = 1; i < argc; i++) {
		if (*argv[i] == '-') {
			strcpy (command, argv[i]); ptr = command;
			while (c = *ptr++) switch (c) {
				case 'd': debug++;			break;
				case '@': control = *ptr++;		*ptr = '\0'; break;
			}
		}
		else switch (k) {
			case 0:	strncpy (format, argv[i], 1023);	k++; break;
			case 1:	getroot (argv[i], imgroot);
				conc_flag = (strstr (argv[i], ".conc") == argv[i] + strlen (imgroot));
									k++; break;
		}	
	}
	if (k < 2) usage (program);

/****************/
/* parse format */
/****************/
	if (!(format = (char *) realloc (format, (1024)*sizeof (char)))) errm (program);
	if (k = expandf (format, 1024)) exit (-1);
	printf ("%s\n", format);
	npts = strlen (format);
	for (nnez = k = 0; k < npts; k++) if (format[k] != 'x') nnez++;
	printf ("%s: time series defined for %d frames, %d exluded\n", program, npts, npts - nnez);

/*****************************/
/* get 4dfp stack dimensions */
/*****************************/
	if (conc_flag) {
		conc_init (&conc_block, program);
		conc_open (&conc_block, imgroot);
		strcpy (imgfile, conc_block.lstfile);
		for (k = 0; k < 4; k++) imgdim[k] = conc_block.imgdim[k];
		isbig = conc_block.isbig;
	} else {
		sprintf (imgfile, "%s.4dfp.img", imgroot);
		if (Getifh (imgfile, &ifhimg)) errr (program, imgfile);
		for (k = 0; k < 4; k++) imgdim[k] = ifhimg.matrix_size[k];
		isbig = strcmp (ifhimg.imagedata_byte_order, "littleendian");
		if (!(imgfp = fopen (imgfile, "rb"))) errr (program, imgfile);
		printf ("Reading: %s\n", imgfile);
	}
	if (!control) control = (isbig) ? 'b' : 'l';
	vdim = imgdim[0] * imgdim[1] * imgdim[2];
	if (imgdim[3] < npts) {
		fprintf (stderr, "%s: more defined npts (%d) than frames (%d)\n", program, npts, imgdim[3]);
		exit (-1);
	}


	if (!(lambda = (double *) calloc (nnez, sizeof (double)))) errm (program);
	v    = calloc_double2 (nnez, nnez);
	imgb = calloc_double2 (vdim, nnez);
	imge = calloc_double2 (vdim, nnez);
	if (!(imga = (float *) calloc (vdim, sizeof (float)))) errm (program);
	if (!(imgv = (float *) calloc (vdim, sizeof (float)))) errm (program);
	if (!(imgt = (float *) calloc (vdim, sizeof (float)))) errm (program);
	if (!(imgm = (int *)   calloc (vdim, sizeof (int))))   errm (program);
	for (jndex = 0; jndex < vdim; jndex++) imgm[jndex] = 1;

/*******************/
/* prepare for svd */
/*******************/
		printf ("reading frame");
		for (k = i = 0; i < npts; i++) {
			printf (" %d", i + 1); fflush (stdout);
			if (conc_flag) {
				conc_read_vol (&conc_block, imgt);
			} else {
				if (eread (imgt, vdim, isbig, imgfp)) errr (program, imgfile);
			}
			if (format[i] == 'x') continue;
			for (jndex = 0; jndex < vdim; jndex++) {
				if (isnan (imgt[jndex]) || imgt[jndex] == (float) 1.e-37) imgm[jndex] = 0;
				imga[jndex]	+= imgt[jndex];
				imgv[jndex]	+= imgt[jndex]*imgt[jndex];
				imgb[jndex][k]	=  imgt[jndex];
			}
			k++;
		} printf ("\n");  fflush (stdout);
		for (jndex = 0; jndex < vdim; jndex++) {
			imga[jndex] /= nnez;
			imgv[jndex] /= nnez;
			imgv[jndex] -= imga[jndex]*imga[jndex];
			if (imgv[jndex] < 1.e-6) imgm[jndex] = 0;
		}
/********************************/
/* singular value decomposition */
/********************************/
	   	        dsvdcmp0 (imgb, vdim, nnez, lambda, v);
		nred = ndsvdcmp0 (imgb, vdim, nnez, lambda, v, tol);
		printf ("reduced npts = %d\n", nred);
		printf ("lambda\n");
		for (i = 0; i < nred; i++) printf (" %11.7f", lambda[i]); printf ("\n");;

 	remove (tmpfile);
	free_double2 (v); free_double2 (imgb); free_double2 (imge); free (lambda);
	free (imgt); free (imga); free (imgv); free (imgm);
	free (format);
	exit (status);
}
