PROG	= hesstest
CSRCS	= ${PROG}.c
FSRCS	= fhesstest.f
RMS	= ../librms
LIN	= ../imglin
LOBJS	= ${RMS}/matopr.o ${RMS}/eigen.o ${LIN}/dnormal.o
OBJS	= ${FSRCS:.f=.o} ${CSRCS:.c=.o}
LIBS	= -lm

CFLAGS	= -O -I${RMS}
ifeq (${OSTYPE}, linux)
	CC	= gcc ${CFLAGS}
	FC	= gcc -O -ffixed-line-length-132 -fno-second-underscore -fcray-pointer
	LIBS	= -lm -lgfortran
else
	CC	= cc ${CFLAGS}
	FC	= f77 -O -I4 -e
	LIBS	= -lm
endif

${PROG}: ${OBJS}
	$(FC) -o $@ ${OBJS} ${LOBJS} ${LIBS}

.c.o:
	$(CC) -c $<

.f.o:
	$(FC) -c $<

clean:
	/bin/rm ${OBJS} ${PROG}

release: ${PROG} 
	chmod 771 ${PROG}
	chgrp program ${PROG}
	mv ${PROG} ${RELEASE}

checkout:
	co $(CSRCS) $(FSRCS) 

checkin:
	ci $(CSRCS) $(FSRCS) 
