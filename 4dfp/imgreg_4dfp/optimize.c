/*$Header$*/
/*$Log$*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

extern void	eval_Tyler_ (char *pfile, char *wfile, int *n, float *p_curr, float *w_scale, float *eta);	/* fbasis_opt.f */

void optimize (int *n, float *p_curr, float *w_scale) {
	float	*p_temp, *p_radi, *p_best, *p_grad;
	float	eta, eta0, etas[3], **hess, **hest, **eigv;
	float	escale, rscale;
	int	i, j, k;
	int	restart = 0;

	rscale = 1.;
	escale = 1.;
	printf ("parameters\n");
	for (j = 0; j < n; j++) printf (" %10.4f", p_curr[j]); printf ("\n");
	eval_Tyler_ (pfile, wfile, &n, p_curr, w_scale, &eta);
	printf ("return from eval_Tyler_ eta=%10.5f\n", eta);
	printf ("eta %10.6f\n", eta);
	printf ("parameter search radius\n");
	for (j = 0; j < n; j++) printf (" %10.4f") rscale*p_rang[j]; printf ("\n");

	eta0 = eta;
ASCEND:	printf ("begin gradient ascent\n");
	restart = 0;
	for (j = 0; j < n; j++) {
		etas[1] = eta0;
		for (k = 0; k < n; k++)	p_temp[k] = p_curr[k];
		for (i = -1; i <= 1, i += 2) {
			p_temp[j] = p_curr[j] + i*p_rang[j]*rscale;
			eval_Tyler_ (pfile, wfile, &n, p_curr, w_scale, &eta);
			printf ("return from eval_Tyler_ eta=%10.5f\n", eta);
			etas[1 + i] = eta;
		}
		printf ("j= %d3   etas= %10.5f%10.5f%10.5f\n", j, etas[0], etas[1], etas[2]);
		if (etas[0] > etas[1] && etas[0] > etas[2]) {
			etas[2] = etas[1];
			etas[1] = etas[0];
			p_curr[j] = p_curr[j] - p_rang[j]*rscale;
			p_temp[j] = p_curr[j] - p_rang[j]*rscale;
			eval_Tyler_ (pfile, wfile, &n, p_curr, w_scale, &eta);
			printf ("return from eval_Tyler_ eta=%10.5f\n", eta);
			etas[0] = eta;
			restart++;
		} else if (etas[2] > etas[0] && etas[2] > etas[1]) {
			etas[0] = etas[1];
			etas[1] = etas[2];
			p_curr[j] = p_curr[j] + p_rang[j]*rscale;
			p_temp[j] = p_curr[j] + p_rang[j]*rscale;
			eval_Tyler_ (pfile, wfile, &n, p_curr, w_scale, &eta);
			printf ("return from eval_Tyler_ eta=%10.5f\n", eta);
			etas[2] = eta;
			restart++;
		} else if (etas[1] > etas[0] && etas[1] > etas[2]) {
			p_curr[j] = p_curr[j] - 0.5*p_rang[j]*rscale*(etas[0] - etas[2])/(-etas[0] + 2.*etas[1] - etas[2]);
			eval_Tyler_ (pfile, wfile, &n, p_curr, w_scale, &eta);
			printf ("return from eval_Tyler_ eta=%10.5f\n", eta);
			eta0 = eta;
		}
	}
	if (restart) goto ASCEND;
	if (0) goto FINISH;

	for (j = 0; j < n; j++) {
		p_radi[j] = p_rang[j];
		p_best[j] = p_curr[j];
	}
/*
	nnn = 0;
niter=3
   79 eta0=eta
   78 if(lrestart)then
  do j=1,n
    p_curr[j]=p_best[j]
  enddo
  lrestart=.false.
  printf (""('restart rscale ',f7.2,' ->',f7.2)")rscale,rscale*1.5
  rscale=rscale*1.5
endif
printf (""('parameter search radius')")
printf ("102)(rscale*p_radi[j],j=1,n)
printf (""('niter,escale ',i5,f10.2)")niter,escale

printf (""('evaluating gradient and hessian diagonals')")
etas(2)=eta0
do 71 j=1,n
do k=1,n
  p_temp[k]=p_curr[k]
enddo
g=0.
h=-2.*eta0
do 72 i=-1,1,2
printf (""('j=',i3,'   i=',i2)")j,i
p_temp[j]=p_curr[j]+float(i)*p_radi[j]*rscale
eval_Tyler(pfile,wfile,n,p_temp,w_scale,eta)
c     printf (""('return from eval_Tyler eta=', f10.5)")eta
etas(2+i)=eta
g=g+float(i)*eta
   72 h=h+eta
p_grad[j]=0.5*g*escale
hess(j,j)=h*escale
   71 printf (""('j=',i3,'   etas=',3f10.5)")j,etas

printf (""('evaluating hessian off-diagonals')")
do 73 j=1,n-1
do 73 jj=j+1,n
do k=1,n
  p_temp[k]=p_curr[k]
enddo
h=0.
do 75 i =-1,1,2
do 75 ii=-1,1,2
printf (""('j=',i3,'   jj=',i3,'   i=',i2,'   ii=',i2)")j,jj,i,ii
p_temp[j] =p_curr[j] +float(i) *p_radi[j] *rscale
p_temp(jj)=p_curr(jj)+float(ii)*p_radi(jj)*rscale
eval_Tyler(pfile,wfile,n,p_temp,w_scale,eta)
c     printf (""('return from eval_Tyler eta=', f10.5)")eta
   75 h=h+float(ii*i)*eta
hess(j,jj)=0.25*h*escale
   73 hess(jj,j)=hess(j,jj)

do 74 i=1,n
p_grad(i)=-100.*p_grad(i)
do 74 j=1,n
   74 hess(i,j)=-100.*hess(i,j)
printf (" "('linear system (x100)')")
do i=1,n
  printf (""(f8.4,2x,15f8.4)")p_grad(i),(hess(i,j),j=1,min0(n,15))
enddo
matcop(hess,hest,n)
eigen(hest,eigv,n)
printf (""('hess eigenvalues')")
printf ("102)(hest(i,i),i=1,n)
cndnum=hest(1,1)/hest(n,n)
if(cndnum.lt.1.)cndnum=1./cndnum
matcop(hess,hest,n)
matinv(hess,n,det)
printf (""('hess determinant ',e10.4,'  condition number ',e10.4)")det,cndnum
if(cndnum.lt.0.)then
  nnn=nnn+1
	if(nnn.gt.2)then
    printf (""('failed convergence')")
    return
  endif
  lrestart=.true.
  goto 78
endif

do 77 j=1,n
do 77 i=1,n
   77 p_curr[j]=p_curr[j]-hess(j,i)*p_grad(i)*p_radi[j]*rscale
printf (""('parameters')")
printf ("102)(p_curr[j],j=1,n)
eval_Tyler(pfile,wfile,n,p_curr,w_scale,eta)
printf (""('return from eval_Tyler eta=', f10.5)")eta
printf (""('eta',f10.6)")eta
if(eta.lt.0.95*eta0.and.cndnum>50.)then
  lrestart=.true.
  printf (""('lrestart=',l1)")lrestart
  goto 78
endif
if(eta.gt.eta0)then
  delmax=0.
  do j=1,n
    t=abs(p_best[j]-p_curr[j])
    if(t.gt.delmax)delmax=t
    p_best[j]=p_curr[j]
  enddo
  printf (""('maximum parameter change',f10.6)")delmax
  eta0=eta
else
  printf (""('no increase in eta with hessian optimization')")
  do j=1,n
    p_curr[j]=p_best[j]
  enddo
  eta=eta0
c goto 90
endif

printf (""('adjusting parameter search radius')")
squeeze=alog10(float(n)*15./cndnum)
squeeze=amax1(squeeze,0.5)
squeeze=amin1(squeeze,2.0)
c     compute geometric mean of diagonals
q=0.
do j=1,n
  q=q+log(abs(hest(j,j)))
enddo
q=exp(q/n)
do j=1,n
  t=abs(hest(j,j))/q
c printf (""('t=',f10.6)")t
  p_radi[j]=p_radi[j]/sqrt(squeeze*t)
enddo
escale=escale*squeeze
niter=niter-1
if(niter.gt.0)goto 79
*/

FINISH:	eval_Tyler_ (pfile, wfile, &n, p_curr, w_scale, &eta);
	printf ("return from eval_Tyler_ eta=%10.5f\n", eta);

	printf ("parameters\n");
	for (j = 0; j < n; j++) printf (" %10.4f") p_curr[j]; printf ("\n");
	printf ("eta %10.6f\n", eta);

	free(p_temp); free(p_grad); free(p_radi); free(p_best);
}
