c$Header: /data/petsun4/data1/src_solaris/imgreg_4dfp/RCS/fbasis_opt.f,v 1.3 2012/08/01 00:51:31 avi Exp $
c$Log: fbasis_opt.f,v $
c Revision 1.3  2012/08/01  00:51:31  avi
c before eliminating FORTRAN
c
c Revision 1.2  2012/07/30  06:14:21  avi
c Revision 1.1  2012/07/29  06:55:09  avi
c Initial revision
c
      subroutine fbasis_opt(pfile,wfile,n,p_curr,w_scale,p_rang)
      character*256 pfile, wfile
      real*4 etas(3)
      real*4 p_curr(n)	! curent parameters (input)
      real*4 p_rang(n)	! nominal parameter search range (input)
      real*4 p_temp(n)	! temp parameters copy
      real*4 p_radi(n)	! adjusted parameter search radius
      real*4 p_best(n)	! best parameters
      real*4 p_grad(n)	! objective function gradient
      real*4 hess(n,n)	! hessian matrix
      real*4 hest(n,n)	! temp matrix copy
      real*4 eigv(n,n)	! matrix eigenvectors
      logical*4 lrestart/.false./
      logical*4 ldebug/.true./
      pointer (pp_grad,p_grad),(pp_temp,p_temp),(pp_radi,p_radi),(pp_best,p_best),(phess,hess),(peigv,eigv),(phest,hest)

      pp_temp=malloc(4*n)	! FORTRAN malloc
      pp_grad=malloc(4*n)
      pp_radi=malloc(4*n)
      pp_best=malloc(4*n)
      phess=malloc(4*n*n)
      peigv=malloc(4*n*n)
      phest=malloc(4*n*n)
      if(pp_temp.eq.0.or.pp_grad.eq.0.or.pp_radi.eq.0.or.pp_best.eq.0.or.peigv.eq.0.or.phess.eq.0.or.phest.eq.0)
     &stop 'memory allocation error'

      rscale=1.
      escale=1.
      call eval_Tyler(pfile,wfile,n,p_curr,w_scale,eta)
c     write(*,"('return from eval_Tyler eta=', f10.5)")eta
      write(*,"('parameters')")
      write(*,102)(p_curr(j),j=1,n)
      write(*,"('eta',f10.6)")eta
      write(*,"('parameter search radius')")
      write(*,102)(rscale*p_rang(j),j=1,n)

      eta0=eta
   60 write(*,"('begin gradient ascent')")
      lrestart=.false.
      do 69 j=1,n
      etas(2)=eta0
      do k=1,n
        p_temp(k)=p_curr(k)
      enddo
      do 62 i=-1,1,2
      p_temp(j)=p_curr(j)+float(i)*p_rang(j)*rscale
      call eval_Tyler(pfile,wfile,n,p_temp,w_scale,eta)
c     write(*,"('return from eval_Tyler eta=', f10.5)")eta
   62 etas(2+i)=eta
   63 write(*,"('j=',i3,'   etas=',3f10.5)")j,etas
      if (etas(1).gt.etas(2).and.etas(1).gt.etas(3))then
        etas(3)=etas(2)
        etas(2)=etas(1)
	p_curr(j)=p_curr(j)-p_rang(j)*rscale
        p_temp(j)=p_curr(j)-p_rang(j)*rscale
        call eval_Tyler(pfile,wfile,n,p_temp,w_scale,eta)
c       write(*,"('return from eval_Tyler eta=', f10.5)")eta
        etas(1)=eta
        lrestart=.true.
        goto 63
      endif
      if (etas(3).gt.etas(2).and.etas(3).gt.etas(1))then
        etas(1)=etas(2)
        etas(2)=etas(3)
	p_curr(j)=p_curr(j)+p_rang(j)*rscale
        p_temp(j)=p_curr(j)+p_rang(j)*rscale
        call eval_Tyler(pfile,wfile,n,p_temp,w_scale,eta)
c       write(*,"('return from eval_Tyler eta=', f10.5)")eta
        etas(3)=eta
        lrestart=.true.
        goto 63
      endif
      if (etas(2).gt.etas(1).and.etas(2).gt.etas(3))then
        p_curr(j)=p_curr(j)-0.5*p_rang(j)*rscale*(etas(1)-etas(3))/(-etas(1)+2.*etas(2)-etas(3))
        call eval_Tyler(pfile,wfile,n,p_curr,w_scale,eta)
c       write(*,"('return from eval_Tyler eta=', f10.5)")eta
        eta0=eta
      endif
   69 continue
      if(lrestart)goto 60
c     goto 90

      do j=1,n
        p_radi(j)=p_rang(j)
        p_best(j)=p_curr(j)
      enddo
      nnn=0
      niter=3
   79 eta0=eta
   78 if(lrestart)then
        do j=1,n
          p_curr(j)=p_best(j)
        enddo
        lrestart=.false.
        write(*,"('restart rscale ',f7.2,' ->',f7.2)")rscale,rscale*1.5
        rscale=rscale*1.5
      endif
      write(*,"('parameter search radius')")
      write(*,102)(rscale*p_radi(j),j=1,n)
      write(*,"('niter,escale ',i5,f10.2)")niter,escale

      write(*,"('evaluating gradient and hessian diagonals')")
      etas(2)=eta0
      do 71 j=1,n
      do k=1,n
        p_temp(k)=p_curr(k)
      enddo
      g=0.
      h=-2.*eta0
      do 72 i=-1,1,2
      write(*,"('j=',i3,'   i=',i2)")j,i
      p_temp(j)=p_curr(j)+float(i)*p_radi(j)*rscale
      call eval_Tyler(pfile,wfile,n,p_temp,w_scale,eta)
c     write(*,"('return from eval_Tyler eta=', f10.5)")eta
      etas(2+i)=eta
      g=g+float(i)*eta
   72 h=h+eta
      p_grad(j)=0.5*g*escale
      hess(j,j)=h*escale
   71 write(*,"('j=',i3,'   etas=',3f10.5)")j,etas

      write(*,"('evaluating hessian off-diagonals')")
      do 73 j=1,n-1
      do 73 jj=j+1,n
      do k=1,n
        p_temp(k)=p_curr(k)
      enddo
      h=0.
      do 75 i =-1,1,2
      do 75 ii=-1,1,2
      write(*,"('j=',i3,'   jj=',i3,'   i=',i2,'   ii=',i2)")j,jj,i,ii
      p_temp(j) =p_curr(j) +float(i) *p_radi(j) *rscale
      p_temp(jj)=p_curr(jj)+float(ii)*p_radi(jj)*rscale
      call eval_Tyler(pfile,wfile,n,p_temp,w_scale,eta)
c     write(*,"('return from eval_Tyler eta=', f10.5)")eta
   75 h=h+float(ii*i)*eta
      hess(j,jj)=0.25*h*escale
   73 hess(jj,j)=hess(j,jj)

      do 74 i=1,n
      p_grad(i)=-100.*p_grad(i)
      do 74 j=1,n
   74 hess(i,j)=-100.*hess(i,j)
      write(*, "('linear system (x100)')")
      do i=1,n
        write(*,"(f8.4,2x,15f8.4)")p_grad(i),(hess(i,j),j=1,min0(n,15))
      enddo
      call matcop(hess,hest,n)
      call eigen(hest,eigv,n)
      write(*,"('hess eigenvalues')")
      write(*,102)(hest(i,i),i=1,n)
      cndnum=hest(1,1)/hest(n,n)
      if(cndnum.lt.1.)cndnum=1./cndnum
      call matcop(hess,hest,n)
      call matinv(hess,n,det)
      write(*,"('hess determinant ',e10.4,'  condition number ',e10.4)")det,cndnum
      if(cndnum.lt.0.)then
        nnn=nnn+1
	if(nnn.gt.2)then
          write(*,"('failed convergence')")
          return
        endif
        lrestart=.true.
        goto 78
      endif

      do 77 j=1,n
      do 77 i=1,n
   77 p_curr(j)=p_curr(j)-hess(j,i)*p_grad(i)*p_radi(j)*rscale
      write(*,"('parameters')")
      write(*,102)(p_curr(j),j=1,n)
      call eval_Tyler(pfile,wfile,n,p_curr,w_scale,eta)
      write(*,"('return from eval_Tyler eta=', f10.5)")eta
      write(*,"('eta',f10.6)")eta
      if(eta.lt.0.95*eta0.and.cndnum>50.)then
        lrestart=.true.
        write(*,"('lrestart=',l1)")lrestart
        goto 78
      endif
      if(eta.gt.eta0)then
        delmax=0.
        do j=1,n
          t=abs(p_best(j)-p_curr(j))
          if(t.gt.delmax)delmax=t
          p_best(j)=p_curr(j)
        enddo
        write(*,"('maximum parameter change',f10.6)")delmax
        eta0=eta
      else
        write(*,"('no increase in eta with hessian optimization')")
        do j=1,n
          p_curr(j)=p_best(j)
        enddo
        eta=eta0
c       goto 90
      endif

      write(*,"('adjusting parameter search radius')")
      squeeze=alog10(float(n)*15./cndnum)
      squeeze=amax1(squeeze,0.5)
      squeeze=amin1(squeeze,2.0)
c     compute geometric mean of diagonals
      q=0.
      do j=1,n
        q=q+log(abs(hest(j,j)))
      enddo
      q=exp(q/n)
      do j=1,n
        t=abs(hest(j,j))/q
c       write(*,"('t=',f10.6)")t
        p_radi(j)=p_radi(j)/sqrt(squeeze*t)
      enddo
      escale=escale*squeeze
      niter=niter-1
      if(niter.gt.0)goto 79      

   90 call eval_Tyler(pfile,wfile,n,p_curr,w_scale,eta)
      write(*,"('return from eval_Tyler eta=', f10.5)")eta
      write(*,"('parameters')")
      write(*,102)(p_curr(j),j=1,n)
      write(*,"('eta',f10.6)")eta

      call free(pp_temp)	! FORTRAN free
      call free(pp_grad)
      call free(pp_radi)
      call free(pp_best)
      call free(phess)
      call free(peigv)
      call free(phest)
      return
  102 format(6f10.4)
      end

      subroutine eval_Tyler(pfile,wfile,n,p_curr,w_scale,eta)
      character*256 pfile,wfile
      character*256 command,tfile
      real*4 p_curr(n)
      external character char

      m=index(wfile,char(0))-1
      if(m.le.0)m=lnblnk(wfile)
      tfile=wfile(1:m)//'.tmp'
      open(8,file=tfile(1:m+4),status='replace',err=99)
      write(8,"('Echo Spacing: ',f10.5)")p_curr(n)
      write(8,"('Weights: ',i3)")n-1
      do 1 i=1,n-1
    1 write(8,"(f16.4)")p_curr(i)*w_scale
      close(8)

      l=index(pfile,char(0))-1
      if(l.le.0)l=lnblnk(pfile)
      write(command,"('/home/usr/blazeyt/scripts/unwarp_eta.py ',a,' ',a)")pfile(1:l),tfile(1:m+4)
c     write(*,"(a)")command
      call system(command)
      open(8,file=tfile(1:m+4),status='old',err=99)
   18 read(8,"(a)",end=99)command
      k=lnblnk(command)
c     write(*,"(a)")command
      if(command(1:4).ne.'Eta:')goto 18
      read(command(5:k),"(f32.4)",err=99)eta
      close(8)
c     write(*,"('eta=',f10.5)")eta
      return
   99 call exit(-1)
      end
