int x4dfp2ecat (float *imag, int *dim, int orientation) {
	switch (orientation) {
		case 2:	flipz_ (imag, dim+0, dim+1, dim+2);	/* transverse */
		case 3:	flipx_ (imag, dim+0, dim+1, dim+2);	/* coronal */
		case 4: break;					/* sagittal */
		default: return -1;				/* none of the above */
	}
	return 0;
}
