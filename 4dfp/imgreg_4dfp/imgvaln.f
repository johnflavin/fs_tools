      subroutine imgvaln(imgt,nx,ny,nz,center,mmppix,x,v,lslice)
c     rimmon sachs  04.08.05 
c     returns in v the nearest neighbor interpolated value of imgt at locus x.
c     lslice returns the slice from which most of the data is taken.
c     lslice = 0 and v = 0. if imgt is undefined at locus x.

      real*4 imgt(nx,ny,nz)
      real*4 center(3),mmppix(3),x(3),xf(3)
      logical*4 ldef,lok
      integer*4 ix(3),mx(3)

      mx(1)=nx
      mx(2)=ny
      mx(3)=nz

      ldef=.true.
      do 1 k=1,3
         center(k)=sign(center(k),mmppix(k))
         xf(k)=(center(k)+x(k))/mmppix(k)
         ix(k)=nint(xf(k))
         ldef=ldef.and.ix(k).gt.0.and.ix(k).le.mx(k)
1     continue

      v=imgt(ix(1),ix(2),ix(3))
      lok=(v.ge.0.0.or.v.le.0.0)
      lslice=ix(3)
      if(.not.(ldef.and.lok))then
          v=0.
          lslice=0
          return
      endif

      return
      end

