/*$Id: hesstest.c,v 1.1 2012/07/28 22:09:16 avi Exp avi $*/
/*$Log: hesstest.c,v $
 * Revision 1.1  2012/07/28  22:09:16  avi
 * Initial revision
 **/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#define MAXL	256
#define D	5	/* dimensionality of linear system */
#define M	10	/* dimensionality of matrix A */

/***********/
/* globals */
/***********/
static char		program[MAXL];

/*************/
/* externals */
/*************/
extern void hesstest_ (float *quad, int *n, float *param, float *p_rang);	/* fhesstest.f */
extern double dnormal (void);							/* dnormal.c */

void errm (char* program) {
	fprintf (stderr, "%s: memory allocation error\n", program);
	exit (-1);
}

float **calloc_float2 (int n1, int n2) {
	int	i;
	float	**a;

	if (!(a = (float **) malloc (n1 * sizeof (float *)))) errm (program);
	if (!(a[0] = (float *) calloc (n1 * n2, sizeof (float)))) errm (program);
	for (i = 1; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_float2 (float **a) {
	free (a[0]);
	free (a);
}

void setprog (char *program, char **argv) {
	char *ptr;

	if (!(ptr = strrchr (argv[0], '/'))) ptr = argv[0]; 
	else ptr++;
	strcpy (program, ptr);
}

int main (int argc, char **argv) {
/**************/
/* processing */
/**************/
	float		*param, *p_rang, **quad, **A;
	int		n = D;
/***********/
/* utility */
/***********/
	char		command[MAXL];
	int		i, j, k;
 
	setprog (program, argv);
/************************/
/* process command line */
/************************/
	for (k = 0, i = 1; i < argc; i++) {
		if (*argv[i] == '-') strcpy (command, argv[i]);
		 else switch (k) {
		}
	}
	if (k < 0) {
		exit (1);
	}

	if (!(param =  (float *) calloc (D, sizeof (float)))) errm (program);
	if (!(p_rang = (float *) calloc (D, sizeof (float)))) errm (program);
	quad = calloc_float2 (D, D);
	A    = calloc_float2 (D, M);
	for (j = 0; j < D; j++) for (k = 0; k < M; k++) A[j][k] = dnormal();
	if (1) {
		for (i = 0; i < D; i++) for (j = 0; j < D; j++) {
			for (k = 0; k < M; k++) quad[i][j] += A[i][k]*A[j][k];
			quad[i][j] /= D;
		}
	} else {
		for (i = 0; i < D; i++) quad[i][i] = 1.;
		quad[1][0] =  0.1;
		quad[0][1] = -0.2;
	}
	printf ("quad\n");
	for (i = 0; i < D; i++) {
		for (j = 0; j < D; j++) printf ("%10.4f", quad[i][j]);
		printf ("\n");
	}

	for (i = 0; i < D; i++) param[i] = p_rang[i] = 0.1;
if (1)	hesstest_ (quad[0], &n, param, p_rang);

	free_float2 (quad); free_float2 (A);
	free (param); free (p_rang);
	exit (0);
}
