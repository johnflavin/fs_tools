#$Header$
#$Log$

PROG	= dsvdcmp_test
CSRCS	= ${PROG}.c dsvdcmp0.c
OBJS	= ${CSRCS:.c=.o} ${FSRCS:.f=.o}
LOBJS	= ../imglin/dnormal.o

CFLAGS	= -O -I.
ifeq (${OSTYPE}, linux)
	CC	= gcc ${CFLAGS}
	FC	= gcc -O -ffixed-line-length-132 -fcray-pointer
	LIBS	= -lm -lgfortran
endif
ifeq (${OSTYPE}, unix)
	CC	= cc ${CFLAGS}
	FC	= f77 -O -I4 -e
	LIBS	= -lm
endif

.c.o:
	${CC} -c $<

.f.o:
	${FC} -c $<

${PROG}: $(OBJS)
	${FC} -o $@ ${OBJS} ${LOBJS} ${LIBS}

clean:
	rm ${OBJS} ${PROG}

checkout:
	co ${CSRCS} ${FSRCS}

checkin:
	ci ${CSRCS} ${FSRCS}

release: ${PROG}
	chmod 711 ${PROG}
	chgrp program ${PROG}
	/bin/mv ${PROG} ${RELEASE}
