#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main()
{
	int i, j, nr, nv, NVOX[256];
	char **RNAME=NULL;
	char line1[512];
	float val[256], tmp;
	float *fptr1, *fptr2;
	FILE *fp1;
	IMAGE_4dfp *Mask=NULL, *phan=NULL;
	
	RNAME=(char **)malloc((size_t)(256*sizeof(char *)));
	if (!RNAME) nrerror("malloc error");
	for (i=0;i<256;i++) {
		RNAME[i]=(char *)malloc((size_t)(256*sizeof(char)));
		if (!RNAME[i]) nrerror("malloc error");
	}
	
	if (! (fp1=fopen("ROIREF", "r"))) nrerror("file open error");

	
	nr=0;
	while (fgets(line1, 512, fp1))
	{
		sscanf(line1, "%s%d%f", RNAME[nr], NVOX+nr, val+nr);
		nr++;
	}
	fclose(fp1);
	
	Mask=read_image_4dfp("RSFMask1", Mask);
	phan=(IMAGE_4dfp *)malloc((size_t)sizeof(IMAGE_4dfp));
	if (!phan) errm("phantom");
	nv=Mask->ifh.matrix_size[0]*Mask->ifh.matrix_size[1]*Mask->ifh.matrix_size[2]*Mask->ifh.matrix_size[3];
	phan->image=(float *)malloc((size_t)(nv*sizeof(float)));
	if (!phan->image) errm("phantom");
	strcpy(phan->ifh.interfile, Mask->ifh.interfile);
	strcpy(phan->ifh.version_of_keys, Mask->ifh.version_of_keys);
	strcpy(phan->ifh.conversion_program, Mask->ifh.conversion_program);
	strcpy(phan->ifh.name_of_data_file, "PIBphantom");
	strcpy(phan->ifh.number_format, Mask->ifh.number_format);
	strcpy(phan->ifh.imagedata_byte_order, Mask->ifh.imagedata_byte_order);
	phan->ifh.number_of_bytes_per_pixel=Mask->ifh.number_of_bytes_per_pixel;
	phan->ifh.number_of_dimensions=Mask->ifh.number_of_dimensions;
	for (i=0; i<3; i++)
	{
		phan->ifh.matrix_size[i]=Mask->ifh.matrix_size[i];
		phan->ifh.scaling_factor[i]=Mask->ifh.scaling_factor[i];
		phan->ifh.mmppix[i]=Mask->ifh.mmppix[i];
		phan->ifh.center[i]=Mask->ifh.center[i];
	}
	phan->ifh.matrix_size[3]=Mask->ifh.matrix_size[3];
	phan->ifh.scaling_factor[3]=Mask->ifh.scaling_factor[3];
	phan->ifh.orientation=Mask->ifh.orientation;
	
	fptr1=Mask->image;
	fptr2=phan->image;
	for (i=0;i<nv;i++)
	{
		tmp=rint((double)*fptr1);
		*fptr2=val[(int)tmp];
		fptr1++;
		fptr2++;
	}
	write_image_4dfp("PIBphantom", phan);
}
