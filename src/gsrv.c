#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <nrutil.h>

int main()
{
	int i, j, nr, NVOX[256];
	char **RNAME=NULL;
	char line[512];
	float val[512], tmp;
	FILE *fp;
	
	RNAME=(char **)malloc((size_t)(256*sizeof(char *)));
	if (!RNAME) nrerror("malloc error");
	for (i=0;i<256;i++) {
		RNAME[i]=(char *)malloc((size_t)(256*sizeof(char)));
		if (!RNAME[i]) nrerror("malloc error");
	}
	if (! (fp=fopen("PIB330late_RSF_ROI_f4954", "r"))) nrerror("file open error");
	fgets(line, 512, fp);
	nr=0;
	while (fgets(line, 512, fp))
	{
		sscanf(line, "%s%d%f", RNAME[nr], NVOX+nr, val+nr);
		nr++;
	}
	fclose(fp);
	for (i=0; i<nr; i++)
	{
		if (strstr(RNAME[i],"Left-")){
			for (j=i+1;j<nr; j++)
			{
				if (strcmp(RNAME[i]+5,RNAME[j]+6)==0) {
					tmp=(val[i]*(float)NVOX[i]+val[j]*(float)NVOX[j])/((float)NVOX[i]+(float)NVOX[j]);
					val[i]=tmp;
					val[j]=tmp;
				}
			}
		}
		
		if (strstr(RNAME[i],"ctx-lh-")){
			for (j=i+1;j<nr; j++)
			{
				if (strcmp(RNAME[i]+7,RNAME[j]+7)==0) {
					tmp=(val[i]*(float)NVOX[i]+val[j]*(float)NVOX[j])/((float)NVOX[i]+(float)NVOX[j]);
					val[i]=tmp;
					val[j]=tmp;
				}
			}
		}
	}
	
	for (i=0; i<nr; i++) printf("%s\t%d\t%f\n",RNAME[i],NVOX[i],val[i]);
	
} 
