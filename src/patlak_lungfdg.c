/*
patlak_lungfdg fdgpet.4dfp.img fdgpet_sumall.4dfp.img aif.tis fdgpet.tim stframe outfile
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main(int argc, char **argv)
{
	IMAGE_4dfp *fdgpet=NULL, *mask=NULL, *patlakout=NULL;
	FILE *fp;
	float t[MAXL], frd[MAXL], AIF[MAXL], dum2;
	int j, stframe, dum1;
	char line[512]; 
	clock_t start, end;
	
	/* Read dynamic FDG PET,and mask*/
	fdgpet=read_image_4dfp(argv[1], fdgpet);
	mask=read_image_4dfp(argv[2],mask);
	
	/* Read AIF*/
	fp = fopen(argv[3],"r");
	if (!fp) errr("patlak_lungfdg", argv[3]);
	fgets(line, 512, fp);
	fgets(line, 512, fp);
	j=0;
	while (fgets(line, 512, fp)){
		sscanf(line, "%f%f", t+j, AIF+j);
		j++;
	}
	fclose(fp);
	
	/* Read TIM */
	fp = fopen(argv[4],"r");
	if (!fp) errr("patlak_lungfdg", argv[4]);
	j=0;
	while (fgets(line, 512, fp)){
		sscanf(line, "%d%f%f", &dum1, &dum2, frd+j);
		frd[j]=frd[j]/60.;
		/*printf("%f\n",frd[j]);*/
		j++;
	}
	fclose(fp);
	
	start=clock();
	
	stframe=atoi(argv[5]);
	patlakout=patlakvol(fdgpet, mask, t, AIF, frd, j, stframe, patlakout);
	end=clock();
	
	strcpy(patlakout->ifh.name_of_data_file, argv[6]);
	write_image_4dfp(patlakout->ifh.name_of_data_file, patlakout);
	printf("CPU TIME USED IS %f seconds.\n", ((double) (end - start)) / CLOCKS_PER_SEC);

}
