/*
This Program scan through multi-frame PET images and determines the frame 
range of 9 parts (a1 a2 a3 b1 b2 b3 c1 c2 and c3).

USAGE:
	DPETTB PETfile model_start_time model_duration
	PETfile is a 4dfp image file
	model_start_time is the starting time point where subsequent 
	modeling will use.
	model_duration is the duration of for the subsequent modeling 
	step
	modeling refers either to logan graphical analysis or SUVR 
	calculation.

Yi Su, 2011-10-25	

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>
void readinfo(char *fn, float *st, float *t, float *frd, float *df, int *frame, int *nf)
{
	FILE *fp;
	char line[MAXL], dummy[MAXL];
	float fdum, *fptr1, *fptr2, *fptr3, *fptr4;
	int i, *iptr5;
	
	if (!(fp=fopen(fn, "r"))) errr("readinfo",fn);
	fptr1=st;
	fptr2=t;
	fptr3=frd;
	fptr4=df;
	iptr5=frame;
	*nf=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f",fptr1, fptr2, fptr3, fptr4, iptr5);
		fptr1++;
		fptr2++;
		fptr3++;
		fptr4++;
		iptr5++;
		(*nf)++;
	}
	fclose(fp);
}

int main(int argc, char **argv)
{
float st[MAXL], t[MAXL], frd[MAXL], df[MAXL], big, mindiff, diff, mst, mdt, met, dt1, dt2, ts, t0;
int frame[MAXL], nf, i, j, startframe[9], endframe[9], vflag;
char fn[MAXL];

strcpy(fn, argv[1]);
mst=atof(argv[2]);
mdt=atof(argv[3]);
mst=mst*60;
mdt=mdt*60;
met=mst+mdt;
dt1=mst/6.;
dt2=mdt/3.;

readinfo(fn, st, t, frd, df, frame, &nf);
if (nf<9) exit(-1);

big=3600;

/* startframe[0] (a1) */
startframe[0]=1;

/* Determine startframe[6] (c1) */
mindiff=big;
t0=st[0];
for(i=0; i<nf; i++)
{
	st[i]=st[i]-t0;
	diff=fabs(st[i]-mst);
	if (diff<mindiff)
	{	
		mindiff=diff;
		startframe[6]=i+1;
	}
/*	printf("%f\n",st[i]); */
}
endframe[5]=startframe[6]-1;

/* Determine startframe[1] (a2) */
i=1;
ts=dt1;
mindiff=big;
while (i<nf)
{
	diff=fabs(st[i]-ts);
	if (diff<mindiff)
	{
		mindiff=diff;
		startframe[1]=i+1;
	}
	i++;
}
endframe[0]=startframe[1]-1;

/* Determine startframe[2] (a3) */
i=startframe[1];
ts=st[i-1]+dt1;
mindiff=big;
while (i<nf)
{
	diff=fabs(st[i]-ts);
	if (diff<mindiff)
	{
		mindiff=diff;
		startframe[2]=i+1;
	}
	i++;
}
endframe[1]=startframe[2]-1;

/* Determine startframe[3] (b1) */
i=startframe[2];
ts=st[i-1]+dt1;
mindiff=big;
while (i<nf)
{
	diff=fabs(st[i]-ts);
	if (diff<mindiff)
	{
		mindiff=diff;
		startframe[3]=i+1;
	}
	i++;
}
endframe[2]=startframe[3]-1;

/* Determine startframe[4] (b2) */
i=startframe[3];
ts=st[i-1]+dt1;
mindiff=big;
while (i<nf)
{
	diff=fabs(st[i]-ts);
	if (diff<mindiff)
	{
		mindiff=diff;
		startframe[4]=i+1;
	}
	i++;
}
endframe[3]=startframe[4]-1;

/* Determine startframe[5] (b3) */
i=startframe[4];
ts=st[i-1]+dt1;
mindiff=big;
while (i<nf)
{
	diff=fabs(st[i]-ts);
	if (diff<mindiff)
	{
		mindiff=diff;
		startframe[5]=i+1;
	}
	i++;
}
endframe[4]=startframe[5]-1;

/* Determine startframe[7] (c2) */
i=startframe[6];
ts=st[i-1]+dt2;
mindiff=big;
while (i<nf)
{
	diff=fabs(st[i]-ts);
	if (diff<mindiff)
	{
		mindiff=diff;
		startframe[7]=i+1;
	}
	i++;
}
endframe[6]=startframe[7]-1;

/* Determine startframe[8] (c3) */
i=startframe[7];
ts=st[i-1]+dt2;
mindiff=big;
while (i<nf)
{
	diff=fabs(st[i]-ts);
	if (diff<mindiff)
	{
		mindiff=diff;
		startframe[8]=i+1;
	}
	i++;
}
endframe[7]=startframe[8]-1;

/* Determine endframe[8] (c3) */
i=startframe[8]-1;
mindiff=big;
while (i<nf)
{
	diff=fabs(st[i]+frd[i]-met);
	if (diff<mindiff)
	{
		mindiff=diff;
		endframe[8]=i+1;
	}
	i++;
}

/* Validity Check */
vflag=0;
if (startframe[6]<=startframe[5]) vflag=1;
if (endframe[8]<startframe[8]) vflag=1;
for (i=0; i<9; i++)
{
	if (startframe[i]>nf || startframe[i]<1) vflag=1;
	if (endframe[i]>nf || endframe[i]<1) vflag=1;
}
if (vflag>0) return(-1);

printf("startframe=( %d %d %d %d %d %d %d %d %d )\n", startframe[0], startframe[1], startframe[2], startframe[3], startframe[4], startframe[5],startframe[6], startframe[7], startframe[8]);
printf("lastframe=( %d %d %d %d %d %d %d %d %d )\n", endframe[0], endframe[1], endframe[2], endframe[3], endframe[4], endframe[5],endframe[6], endframe[7], endframe[8]);
return(0);
}
