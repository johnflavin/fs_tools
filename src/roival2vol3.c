#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main(int argc, char **argv)
{
	int i, j, col, lr, nvroi[16384], m2, vallistl[256],vallistr[256], nrois, nroit, nr, n, nx, ny, nz, *val, tmp;
	float *fptr1, *fptr2, *fptr3, cmppix[3], val1[256], val2[256], val3[256], val4[256], val5[256], *vt;
	IMAGE_4dfp *FSdata=NULL, *mask=NULL;
	FILE *fp;
	char **fsregions = NULL;
	char **fsregiont = NULL;
	char optf[MAXL], tmpstr[MAXL], roimeanfile[MAXL],line[512], line1[512];

	/* read FSdata */
	FSdata=read_image_4dfp(argv[1], FSdata);
	
	/* read roi list */
	fp = fopen(argv[2],"r");
	if (!fp) errr("roival2vol2",argv[2]);
	fsregions = (char **)malloc((size_t)((256)*sizeof(char *)));
	if (!fsregions) errm("roival2vol2");
	fgets(line, 512, fp);
	j=0;
	while (fgets(line, 512, fp)){
		sscanf(line, "%s%d", tmpstr, vallistl+j, vallistr+j);
		fsregions[j]=(char *)malloc((size_t)(MAXL*sizeof(char)));
		if (!fsregions[j]) errm("roival2vol3");
		strcpy(fsregions[j], tmpstr);
		j++;  
	}
	nrois=j;
	fclose(fp);
	printf("Done...Read ROI List\n");
	
	/* read roi values */
	strcpy(roimeanfile, argv[3]);
	col=atoi(argv[4]);
	
	if (! (fp=fopen(roimeanfile, "r"))) errr("roival2vol3",roimeanfile);

	switch (col) {
		case 1: vt=val1; /* nvox */
		break;
		case 2: vt=val2; /* bp */
		break;
		case 3: vt=val3; /* bprsf */
		break;
		case 4: vt=val4; /* suvr */
		break;
		case 5: vt=val5; /* suvrrsf */
		break;
		default:
		nrerror("invalid column");
	}

	fgets(line1, 512, fp);

	for (nr=0; nr<nrois; nr++)
	{
		fgets(line1, 512, fp);
		sscanf(line1, "%s%f%f%f%f%f", tmpstr, val1+nr, val2+nr, val3+nr, val4+nr, val5+nr);
		printf("%s\t%f\n",tmpstr,vt[nr]);
	}
	fclose(fp);
	printf("----\n");

		
	
	/* determine fill left or right hemisphere */
	lr=atoi(argv[5]);
	switch (lr) {
		case 1: val=vallistl;
		break;
		case 2: val=vallistr;
		break;
		default:
		nrerror("invalid hemisphere");
	}
	
	/* allocate memory for mask */
	m2=FSdata->ifh.number_of_bytes_per_pixel*FSdata->ifh.matrix_size[0]*FSdata->ifh.matrix_size[1]*FSdata->ifh.matrix_size[2]*FSdata->ifh.matrix_size[3];
	mask=(IMAGE_4dfp *)malloc((size_t)sizeof(IMAGE_4dfp));
	if (!mask) errm("roival2vol");
	mask->image=(float *)malloc((size_t)m2);
	if (!mask->image) errm("roival2vol");	
	m2=m2/FSdata->ifh.number_of_bytes_per_pixel;
	
	fptr1=FSdata->image;
	fptr2=mask->image;
	for (i=0;i<m2;i++) {
		*fptr2=-1.;
		for (j=1; j<nrois; j++) {
			if ((int)(*fptr1)==val[j] 
				&& (int)(*fptr1)!= 4
				&& (int)(*fptr1)!= 5
				&& (int)(*fptr1)!= 7 
				&& (int)(*fptr1)!= 8 
				&& (int)(*fptr1)!= 14
				&& (int)(*fptr1)!= 15				 
				&& (int)(*fptr1)!= 16 
				&& (int)(*fptr1)!= 24
				&& (int)(*fptr1)!= 30
				&& (int)(*fptr1)!= 43
				&& (int)(*fptr1)!= 44				 
				&& (int)(*fptr1)!= 46 
				&& (int)(*fptr1)!= 47
				&& (int)(*fptr1)!= 62
				&& (int)(*fptr1)!= 72
				&& (int)(*fptr1)!= 77
				&& (int)(*fptr1)<3000){
				*fptr2=vt[j];}
			else if ((int)(*fptr1)==val[j] 
				&& ((int)(*fptr1)== 4
				|| (int)(*fptr1)== 5
				|| (int)(*fptr1)== 14
				|| (int)(*fptr1)== 15				 
				|| (int)(*fptr1)== 24
				|| (int)(*fptr1)== 30
				|| (int)(*fptr1)== 43
				|| (int)(*fptr1)== 44				 
				|| (int)(*fptr1)== 62
				|| (int)(*fptr1)== 72
				|| (int)(*fptr1)== 77
				|| (int)(*fptr1)>3000)) {
				*fptr2=-0.5;}

		}
		fptr1++;
		fptr2++;
	}
	
	strcpy(optf,argv[6]);
	strcpy(mask->ifh.interfile, FSdata->ifh.interfile);
	strcpy(mask->ifh.version_of_keys, FSdata->ifh.version_of_keys);
	strcpy(mask->ifh.conversion_program, FSdata->ifh.conversion_program);
	
	strcpy(mask->ifh.number_format, FSdata->ifh.number_format);
	strcpy(mask->ifh.imagedata_byte_order, FSdata->ifh.imagedata_byte_order);
	mask->ifh.number_of_bytes_per_pixel=FSdata->ifh.number_of_bytes_per_pixel;
	mask->ifh.number_of_dimensions=FSdata->ifh.number_of_dimensions;
	for (i=0; i<3; i++)
	{
		mask->ifh.matrix_size[i]=FSdata->ifh.matrix_size[i];
		mask->ifh.scaling_factor[i]=FSdata->ifh.scaling_factor[i];
		mask->ifh.mmppix[i]=FSdata->ifh.mmppix[i];
		mask->ifh.center[i]=FSdata->ifh.center[i];
	}
	mask->ifh.matrix_size[3]=FSdata->ifh.matrix_size[3];
	mask->ifh.scaling_factor[3]=FSdata->ifh.scaling_factor[3];
	mask->ifh.orientation=FSdata->ifh.orientation;
	sprintf(mask->ifh.name_of_data_file, optf);
	write_image_4dfp(mask->ifh.name_of_data_file, mask);
	
}
