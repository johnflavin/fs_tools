#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

bool file_exists(const char * filename)
{
	FILE *file;
	if(file = fopen(filename, "r")) {fclose(file);        return true;   }  return false;
}

float SUVR(float *REF, float *ROI, float *dcf, float *frd, int nframes, int stframe, int endframe)
{
	int i;
	float SUVR, vREF, vROI;
	
	vREF=0;
	vROI=0;
	for (i=stframe-1; i<endframe; i++) {
		vREF+=REF[i]*frd[i]/dcf[i];
		vROI+=ROI[i]*frd[i]/dcf[i];
	}
	SUVR=vROI/vREF;
	return(SUVR);
}

void readPIBtac(char *fn, float *tac, float *rsftac, float *frd, float *st, float *nv, int *nframes)
{
	FILE *fp;
	char line[MAXL], dummy[MAXL];
	float fdum, *fptr1, *fptr2, *fptr3, *fptr4;
	int i;
	
	if (!(fp=fopen(fn, "r"))) errr("ROIPIB","fn");
	fgets(line, 512, fp);
	sscanf(line, "%s%s%s%s%s%s%f", dummy, dummy, dummy, dummy, dummy, dummy, nv);
	fptr3=tac;
	fptr4=rsftac;
	fptr2=frd;
	fptr1=st;
	*nframes=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f", &fdum, fptr1, fptr2, fptr3, fptr4);
		fptr1++;
		fptr2++;
		fptr3++;
		fptr4++;
		(*nframes)++;
	}
	fclose(fp);
}

void readPIBtac2(char *fn, float *tac, float *rsftac, float *pvc2ctac, float *frd, float *st, float *nv, int *nframes)
{
	FILE *fp;
	char line[MAXL], dummy[MAXL];
	float fdum, *fptr1, *fptr2, *fptr3, *fptr4, *fptr5;
	int i;
	
	if (!(fp=fopen(fn, "r"))) errr("ROIPIB1",fn);
	fgets(line, 512, fp);
	sscanf(line, "%s%s%s%s%s%s%s%f", dummy, dummy, dummy, dummy, dummy, dummy, dummy, nv);
	fptr3=tac;
	fptr4=rsftac;
	fptr5=pvc2ctac;
	fptr2=frd;
	fptr1=st;
	*nframes=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f%f", &fdum, fptr1, fptr2, fptr3, fptr4, fptr5);
		fptr1++;
		fptr2++;
		fptr3++;
		fptr4++;
		fptr5++;
		(*nframes)++;
	}
	fclose(fp);
}
int main(int argc, char **argv)
{
	char fn1[MAXL], PID[MAXL], line[MAXL], LCCfn[MAXL], RCCfn[MAXL], **arois, tmpstr[MAXL], f1[MAXL], roitacfn[MAXL];
	int nframes, intval, i, nar, stframe, endframe;
	float *fptr, dcf[MAXL], dummy, ltac[MAXL], rtac[MAXL], tac[MAXL], rsfltac[MAXL], rsfrtac[MAXL], rsftac[MAXL], t[MAXL];
	float nv, nv1, nv2, frd[MAXL], st[MAXL], pvc2c[MAXL], cf1, cf2, REFtac[MAXL], REFrsftac[MAXL], REFpvctac[MAXL], pvctac[MAXL];
	float bp, bpRSF, bppvc, suvr, suvrRSF, suvrpvc, intc, slope, rsq, rsfintc, rsfrsq, pvcintc, pvcrsq, k2=0.16;
	FILE *fp, *fp1;

	strcpy(PID, argv[1]);
	strcpy(roitacfn, argv[2]);
	printf("%s\n", PID);
	printf("%s\n", roitacfn);
	
	/* Read info file for decay correction factor dcf */
	printf("Read .info file for decay correction factor.\n");
	sprintf(fn1, "%s.info", PID);
	if (!(fp=fopen(fn1, "r"))) errr("ROIPIB2",fn1);
	nframes=0;
	fptr=dcf;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f", &dummy, &dummy, &dummy, fptr, &dummy);
		fptr++;
		(nframes)++;
	}
	fclose(fp);
	
	
	/* Read Cerebellum TACs */
	printf("Read Cerebellum TACs\n");
	sprintf(LCCfn, "%s_Left-Cerebellum-Cortex_RSF.tac", PID);
	sprintf(RCCfn, "%s_Right-Cerebellum-Cortex_RSF.tac", PID);
	if (file_exists(LCCfn))
		readPIBtac(LCCfn, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0;
	
	if (file_exists(RCCfn))
		readPIBtac(RCCfn, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0;
	
	if (nv1+nv2==0) {
		printf("Cerebellum Cortex Undefined. Abort!\n");
		exit(-1);
	} else {
		if (nv1==0) {
			for (i=0; i<nframes; i++) {
				ltac[i]=0;
				rsfltac[i]=0;				
			}
		}
		if (nv2==0) {
			for (i=0; i<nframes; i++) {
				rtac[i]=0;
				rsfrtac[i]=0;				
			}
		}
	}
	
	/* Read PVC2C correction factor */
	arois = (char **)malloc((size_t)((512)*sizeof(char *)));
	if (!(fp=fopen("PVC2CFS.txt", "r"))) errr("ROIPIB","PVC2CFS.txt");
	i=0;
	fptr=pvc2c;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%s%f%d", tmpstr, fptr, &intval);
		arois[i]=(char *)malloc((size_t)(MAXL*sizeof(char)));
		if (!arois[i]) errm("ROIPIB");
		strcpy(arois[i], tmpstr);
		i++;
		fptr++;
	}
	nar=i;
	fclose(fp);
		
	/* Generate REFERENCE TAC */
	printf("Generate REF TACs\n");
	for (i=0; i<nar; i++)
	{
		if (strcmp(arois[i],"Left-Cerebellum-Cortex")==0) cf1=pvc2c[i];
		if (strcmp(arois[i],"Right-Cerebellum-Cortex")==0) cf2=pvc2c[i];		
	}
	
	for (i=0; i<nframes; i++)
	{
		REFtac[i]=(ltac[i]*nv1+rtac[i]*nv2)/(nv1+nv2);
		REFrsftac[i]=(rsfltac[i]*nv1+rsfrtac[i]*nv2)/(nv1+nv2);
		REFpvctac[i]=(ltac[i]*nv1/cf1+rtac[i]*nv2/cf2)/(nv1+nv2);
		t[i]=st[i]+frd[i]/2.-st[0];
	}

	/* ROI Modeling */
	printf("Start Modeling\n");
	sprintf(f1, "%s.txt", roitacfn);
	stframe=nframes-5;
	endframe=nframes;
	if (!(fp1 =fopen(f1, "w"))) errw("ROIPIB2", f1);
	if (file_exists(roitacfn))
		readPIBtac(roitacfn, tac, rsftac, frd, st, &nv, &nframes); 
	else
		nv=0;
	if (nv>0) {
		bp=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
		suvr=SUVR(REFtac, tac, dcf, frd, nframes, stframe, endframe);
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f\n", roitacfn, nv, bp, rsq, intc, suvr);	
	}
	fclose(fp1);
}
