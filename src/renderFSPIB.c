#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "AVW.h"
#include "AVW_Render.h"
#include "AVW_ImageFile.h"

int main(int argc, char **argv)
{
	AVW_ImageFile *db;
	AVW_Image *outimg=NULL, *rimg=NULL, *gimg=NULL, *bimg=NULL, *scimg=NULL; 
	AVW_Volume *volume;
	AVW_Colormap *cmap;
	AVW_ObjectMap *object_map=NULL;
	AVW_RenderedImage *rendered=NULL, *rendered2=NULL;
	AVW_RenderParameters *render_param;
	int i, df;
	char imagefile[256], colormapfile[256], outimagefile[256];
	unsigned char *uscr, *uscg, *uscb, *usc1;
	unsigned int *usi;
	float *fptr;
	double ilo, ihi, angle;

	strcpy(imagefile, argv[1]);
	strcpy(colormapfile, argv[2]);
	strcpy(outimagefile, argv[3]);
	df = atoi(argv[4]);
	ilo= atof(argv[5]);
	ihi= atof(argv[6]);
	switch (df) {
		case 1: angle=90.;
		break;
		case 2: angle=-90.;
		break;
		default:
		exit(1);
	}
	
	db = AVW_OpenImageFile(imagefile,"r");
	if(!db) { AVW_Error("AVW_OpenImageFile"); exit(1); }
	volume = AVW_ReadVolume(db, 0, NULL);
	if(!volume) { AVW_Error("AVW_ReadVolume"); exit(1); }
	volume = AVW_FlipVolume(volume, AVW_FLIPY, volume);
	AVW_CloseImageFile(db);
	
	render_param = AVW_InitializeRenderParameters(volume, NULL, NULL);
	if(!render_param) { AVW_Error("AVW_InitializeRenderParameters"); exit(1); }
	render_param->Matrix = AVW_RotateMatrix(render_param->Matrix, -90., angle, 0., render_param->Matrix);
	render_param->ThresholdMinimum = -0.5;
	render_param->BackgroundValue = 255;
	
	fprintf(stdout, "."); fflush(stdout);
	fflush(stdout);
	rendered = AVW_RenderVolume(render_param, rendered);
	if(!rendered) { AVW_Error("AVW_RenderVolume"); exit(1); }
	
	render_param->Type = AVW_SURFACE_PROJECTION;
	render_param->SurfaceThickness = 1;
	render_param->BackgroundValue = 0;
	
	fprintf(stdout, "."); fflush(stdout);
	fflush(stdout);
	rendered2 = AVW_RenderVolume(render_param, rendered2);
	if(!rendered2) { AVW_Error("AVW_RenderVolume"); exit(1); }
	cmap = AVW_LoadColormap(colormapfile);
	if(!cmap) { AVW_Error("AVW_LoadColormap"); exit(1); }
	outimg = AVW_ConvertImage(rendered->Image, AVW_COLOR, outimg);
	if(!outimg) { AVW_Error("AVW_ConvertImage"); exit(1); }
	scimg = AVW_IntensityScaleImage(rendered2->Image, ihi, ilo, (double)(cmap->Size-1), 0., AVW_UNSIGNED_INT, scimg);

	rimg = AVW_GetImageChannel(outimg, AVW_RED_CHANNEL, rimg);
	gimg = AVW_GetImageChannel(outimg, AVW_GREEN_CHANNEL, gimg);
	bimg = AVW_GetImageChannel(outimg, AVW_BLUE_CHANNEL, bimg);
	
	uscr = (unsigned char *) rimg->Mem;
	uscg = (unsigned char *) gimg->Mem;
	uscb = (unsigned char *) bimg->Mem;
	usc1 = (unsigned char *) rendered->Image->Mem;
	usi  = (unsigned int  *) scimg->Mem;
	fptr = (float *) rendered2->Image->Mem;
	
	for (i=0; i<rimg->PixelsPerImage; i++)
		{
		if (*usc1==255) {
			*uscr=255; *uscg=255; *uscb=255;
			} 
		else if (*fptr==-0.5) {
			*uscr=100; *uscg=100; *uscb=100;
			}
		else	{
			*uscr=cmap->Red[*usi];
			*uscg=cmap->Green[*usi];
			*uscb=cmap->Blue[*usi];
			}
		uscr++; uscg++; uscb++; usc1++; usi++; fptr++;
		}
		
	printf("2\n");
	AVW_PutImageChannel(rimg, AVW_RED_CHANNEL, outimg);
	AVW_PutImageChannel(gimg, AVW_GREEN_CHANNEL, outimg);
	AVW_PutImageChannel(bimg, AVW_BLUE_CHANNEL, outimg);
	printf("3\n");
	
	outimg = AVW_BlendImages(outimg, 0.9, rendered->Image, AVW_BLEND_AVG_IGNORE_ZEROS, outimg);
	
	/*	
	if(AVW_ShowImage(outimg) != AVW_SUCCESS)
		{
		AVW_Error("AVW_ShowImage Failed");
		exit(1);
		}
	
	*/
	db = AVW_CreateImageFile(outimagefile,"PNG",outimg->Width, outimg->Height, 1, outimg->DataType);
	
	
	AVW_WriteImageFile(db, outimg);
	AVW_CloseImageFile(db);
	fprintf(stdout, "\n"); fflush(stdout);	
	exit(0);
}
