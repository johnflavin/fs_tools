#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main()
{
	int n, i, j;
	float *ref, *observe, *esti, *fptr, tmp;
	char line[512];
	RSFMat *rsfmat;
	FILE *fp;

	n=177;
	ref=vector(1,n);
	for (i=1;i<=n;i++) ref[i]=(float)i;
	rsfmat = (RSFMat *) malloc((size_t)sizeof(RSFMat));
	if (!rsfmat) errm("rsfpvctest");
	rsfmat->n = n;
	rsfmat->mat = matrix(1,n,1,n);
	
	fptr=rsfmat->mat[1];
	fptr++;
	if (!(fp =fopen("/data/nil-bluearc/mintun/SuY/SUBJECT_64075/pib_fs/RSFMat1.dat", "r"))) errr("rsfpvctest", "RSFMat1.dat");
	while (fgets(line, 512, fp))
	{
		sscanf(line,"%e",fptr);
		fptr++;
	}
	fclose(fp);
	
	observe=vector(1,n);
	
	for (i=1; i<=n; i++){
		observe[i]=0.;
		tmp=0;
		for (j=1; j<=n; j++) {
			tmp+=rsfmat->mat[j][i];
			observe[i]+=ref[j]*rsfmat->mat[j][i]; 
		}
		printf("%f\n",tmp);
	} 
	
	esti=RSFPVC(rsfmat, observe, 20);
	
	for (i=1; i<=n; i++) {
		printf("%f\t%f\t%f\n",ref[i], observe[i], esti[i]);
	}
	free_vector(ref, 1, n);
	free_vector(observe, 1, n);
	free_vector(esti, 1, n);
	free_matrix(rsfmat->mat, 1, n, 1, n);
	free(rsfmat);
}
