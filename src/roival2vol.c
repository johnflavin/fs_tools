#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main(int argc, char **argv)
{
	int i, j, col, nvroi[16384], m2, vallists[16384],vallistt[16384], nrois, nroit, nr, n, nx, ny, nz, tmp;
	float *fptr1, *fptr2, *fptr3, cmppix[3], val1[256], val2[256], val3[256], val4[256], *val, vt[256];
	IMAGE_4dfp *FSdata=NULL, *mask=NULL;
	FILE *fp;
	char **fsregions = NULL;
	char **fsregiont = NULL;
	char optf[MAXL], tmpstr[MAXL], roimeanfile[MAXL],line[512], line1[512];

	/* read FSdata */
	FSdata=read_image_4dfp(argv[1], FSdata);
	
	/* read roi source list */
	fsregions = (char **)malloc((size_t)((16384)*sizeof(char *)));
	if (!fsregions) errm("roival2vol");
	
	fp = fopen(argv[2],"r");
	if (!fp) errr("roival2vol",argv[2]);
	j=0;
	while (fgets(line, 512, fp)){
		sscanf(line, "%s%d", tmpstr, vallists+j);
		fsregions[j]=(char *)malloc((size_t)(MAXL*sizeof(char)));
		if (!fsregions[j]) errm("roival2vol");
		strcpy(fsregions[j], tmpstr);
		j++;  
	}
	nrois=j;
	fclose(fp);
	printf("Done...Read ROI Source List\n");
	
	/* read roi target list */
	fsregiont = (char **)malloc((size_t)((16384)*sizeof(char *)));
	if (!fsregiont) errm("roival2vol");
	
	fp = fopen(argv[3],"r");
	if (!fp) errr("roival2vol",argv[3]);
	j=0;
	while (fgets(line, 512, fp)){
		sscanf(line, "%s%d", tmpstr, vallistt+j);
		fsregiont[j]=(char *)malloc((size_t)(MAXL*sizeof(char)));
		if (!fsregiont[j]) errm("roival2vol");
		strcpy(fsregiont[j], tmpstr);
		j++;  
	}
	nroit=j;
	fclose(fp);
	printf("Done...Read ROI Target List\n");
	
	printf("%d\t%d\n",nrois,nroit);
	/* read roi values */
	strcpy(roimeanfile, argv[4]);
	col=atoi(argv[5]);
	
	if (! (fp=fopen(roimeanfile, "r"))) errr("roival2vol",roimeanfile);

	switch (col) {
		case 1: val=val1;
		break;
		case 2: val=val2;
		break;
		case 3: val=val3;
		break;
		case 4: val=val4;
		break;
		default:
		nrerror("invalid column");
	}

	fgets(line1, 512, fp);

	for (nr=0; nr<nrois; nr++)
	{
		fgets(line1, 512, fp);
		sscanf(line1, "%s%f%f%f%f", tmpstr, val1+nr, val2+nr, val3+nr, val4+nr);
		printf("%s\t%f\n",tmpstr,val[nr]);
	}
	fclose(fp);
	printf("----\n");

	
	/* Map source roi to target roi */
	for (i=1; i<nroit; i++) {
		vt[i]=-1;
		for (j=1; j<nrois; j++) {
			if (vallistt[i]==vallists[j]) {
				vt[i]=val[j];
				printf("%d\t%d\t%f\n",vallistt[i],vallists[j],val[j]);
			}
		}
	}
	
	
	/* allocate memory for mask */
	m2=FSdata->ifh.number_of_bytes_per_pixel*FSdata->ifh.matrix_size[0]*FSdata->ifh.matrix_size[1]*FSdata->ifh.matrix_size[2]*FSdata->ifh.matrix_size[3];
	mask=(IMAGE_4dfp *)malloc((size_t)sizeof(IMAGE_4dfp));
	if (!mask) errm("roival2vol");
	mask->image=(float *)malloc((size_t)m2);
	if (!mask->image) errm("roival2vol");	
	m2=m2/FSdata->ifh.number_of_bytes_per_pixel;
	
	fptr1=FSdata->image;
	fptr2=mask->image;
	for (i=0;i<m2;i++) {
		*fptr2=-1;
		for (j=1; j<nroit; j++) {
			if ((int)(*fptr1)==vallistt[j]) *fptr2=vt[j];
		}
		fptr1++;
		fptr2++;
	}
	
	strcpy(optf,argv[6]);
	strcpy(mask->ifh.interfile, FSdata->ifh.interfile);
	strcpy(mask->ifh.version_of_keys, FSdata->ifh.version_of_keys);
	strcpy(mask->ifh.conversion_program, FSdata->ifh.conversion_program);
	
	strcpy(mask->ifh.number_format, FSdata->ifh.number_format);
	strcpy(mask->ifh.imagedata_byte_order, FSdata->ifh.imagedata_byte_order);
	mask->ifh.number_of_bytes_per_pixel=FSdata->ifh.number_of_bytes_per_pixel;
	mask->ifh.number_of_dimensions=FSdata->ifh.number_of_dimensions;
	for (i=0; i<3; i++)
	{
		mask->ifh.matrix_size[i]=FSdata->ifh.matrix_size[i];
		mask->ifh.scaling_factor[i]=FSdata->ifh.scaling_factor[i];
		mask->ifh.mmppix[i]=FSdata->ifh.mmppix[i];
		mask->ifh.center[i]=FSdata->ifh.center[i];
	}
	mask->ifh.matrix_size[3]=FSdata->ifh.matrix_size[3];
	mask->ifh.scaling_factor[3]=FSdata->ifh.scaling_factor[3];
	mask->ifh.orientation=FSdata->ifh.orientation;
	sprintf(mask->ifh.name_of_data_file, optf);
	write_image_4dfp(mask->ifh.name_of_data_file, mask);
	
}
