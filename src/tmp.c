#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

bool file_exists(const char * filename)
{
	FILE *file;
	if(file = fopen(filename, "r")) {fclose(file);        return true;   }  return false;
}
    
float SUVR(float *REF, float *ROI, float *frd, int nframes, int stframe, int endframe)
{
	int i;
	float SUVR, vREF, vROI;
	
	vREF=0;
	vROI=0;
	for (i=stframe-1; i<endframe; i++) {
		vREF+=REF[i]*frd[i];
		vROI+=ROI[i]*frd[i];
	}
	SUVR=vROI/vREF;
	return(SUVR);
}

void readPIBtac(char *fn, float *tac, float *rsftac, float *frd, float *st, float *nv, int *nframes)
{
	FILE *fp;
	char line[MAXL], dummy[MAXL];
	float fdum, *fptr1, *fptr2, *fptr3, *fptr4;
	int i;
	
	if (!(fp=fopen(fn, "r"))) errr("ROIPIB","ROIs");
	fgets(line, 512, fp);
	sscanf(line, "%s%s%s%s%s%s%f", dummy, dummy, dummy, dummy, dummy, dummy, nv);
	fptr1=tac;
	fptr2=rsftac;
	fptr3=frd;
	fptr4=st;
	nframes=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f", &fdum, fptr1, fptr2, fptr3, fptr4);
		fptr1++;
		fptr2++;
		fptr3++;
		fptr4++;
		nframes++;
	}
	fclose(fp);
}

int main(int argc, char **argv)
{
	char PID[MAXL], line[MAXL], LCCfn[MAXL], RCCfn[MAXL], f1[MAXL], f2[MAXL], f3[MAXL];
	char r1[MAXL], r2[MAXL], r[MAXL], fn1[MAXL], fn2[MAXL], fn3[MAXL], fn4[MAXL], tmpstr[MAXL];
	char **brois = NULL;
	FILE *fp, *fp1, *fp2, *fp3;
	float k2=0.16;
	float bp1, bp2, bp, t[MAXL],  intercept, slope, Rsq, bpPREC, bpGR, bpTEMP, bpPREF, bpRSFPREC, bpRSFGR, bpRSFTEMP, bpRSFPREF;
	float ltac[MAXL], rsfltac[MAXL], frd[MAXL], st[MAXL], nv1, nv2, nv3, nv4, nv, suvr, tac3[MAXL], rsftac3[MAXL], tac4[MAXL], rsftac4[MAXL];
	float SUVR1, SUVR2, SUVRRSF1, SUVRRSF2, SUVRRSF, bpRSF1, bpRSF2, bpRSF;
	float suvrPREC, suvrGR, suvrTEMP, suvrPREF, SUVRRSFPREC, SUVRRSFGR, SUVRRSFTEMP, SUVRRSFPREF;
	float MCBP, MCBPRSF, MCBPSUVR, MCBPSUVRRSF;
	float rtac[MAXL], rsfrtac[MAXL], REFtac[MAXL], REFrsftac[MAXL], tac[MAXL], rsftac[MAXL];
	int i, rflag[MAXL], nbr, stframe, endframe, nframes, intval;
	
	strcpy(PID, argv[1]);
	brois = (char **)malloc((size_t)((256)*sizeof(char *)));
	if (!(fp=fopen("/data/nil-bluearc/benzinger2/suy/ROIs", "r"))) errr("ROIPIB","ROIs");
	i=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%s%d", tmpstr, intval);
		brois[i]=(char *)malloc((size_t)(MAXL*sizeof(char)));
		if (!brois[i]) errm("ROIPIB");
		strcpy(brois[i], tmpstr);
		rflag[i]=intval;
		i++;
	}
	nbr=i;
	fclose(fp);
	
	/* Read Cerebellum TACs */
	sprintf(LCCfn, "%s_Left-Cerebellum-Cortex_RSF.tac", PID);
	sprintf(RCCfn, "%s_Right-Cerebellum-Cortex_RSF.tac", PID);
	if (file_exists(LCCfn))
		readPIBtac(LCCfn, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0;
	
	if (file_exists(RCCfn))
		readPIBtac(RCCfn, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0;
	
	if (nv1+nv2==0) {
		printf("Cerebellum Cortex Undefined. Abort!\n");
		exit(-1);
	} else {
		if (nv1==0) {
			for (i=0; i<nframes; i++) {
				ltac[i]=0;
				rsfltac[i]=0;				
			}
		}
		if (nv2==0) {
			for (i=0; i<nframes; i++) {
				rtac[i]=0;
				rsfrtac[i]=0;				
			}
		}
	}
		
	/* Generate REFENCE TAC */
	for (i=0; i<nframes; i++)
	{
		REFtac[i]=(ltac[i]*nv1+rtac[i]*nv2)/(nv1+nv2);
		REFrsftac[i]=(rsfltac[i]*nv1+rsfrtac[i]*nv2)/(nv1+nv2);
		t[i]=st[i]+frd[i]/2.-st[0];
	}
	
	/* ROI Modeling */
	sprintf(f1, "%s_ROIPIB.txt", PID);
	sprintf(f2, "%s_ROIPIBLR.txt",PID);
	
	if (!(fp1 =fopen(f1, "w"))) errw("ROIPIB", f1);
	if (!(fp2 =fopen(f2, "w"))) errw("ROIPIB", f2);
	fprintf(fp1, "%-35s %11s %11s %11s %11s %11s\n", );
	fprintf(fp2, "%11.4f %11.4f %11.4f\n" $mcbp $mcbpRSF $mcbpPVC2C);
	
	for (i=0; i<54; i++)
	{
		switch(rflag[i]){
			case 1:
				sprintf(fn1, "%s_Left-%s_RSF.tac",PID,brois[i]);
				sprintf(fn2, "%s_Right-%s_RSF.tac",PID,brois[i]);
				sprintf(r1, "Left-%s", brois[i]);
				sprintf(r2, "Right-%s", brois[i]);
				sprintf(r, "%s", brois[i]);
				
				if (file_exists(fn1))
					readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
				else
					nv1=0;
	
				if (file_exists(fn2))
					readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
				else
					nv2=0;
	
				if (nv1+nv2==0) {
					nv=0;
				} else {
					if (nv1==0) {
						for (i=0; i<nframes; i++) {
							ltac[i]=0;
							rsfltac[i]=0;				
						}
						bp1=0.;
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0.);
					} else {
						bp1=loganREF(REFtac, ltac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						bpRSF1=loganREF(REFrsftac, rsfltac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						SUVR1=SUVR(REFtac, ltac, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfltac, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, bp1, bpRSF1, SUVR1, SUVRRSF1);
					}
					if (nv2==0) {
						for (i=0; i<nframes; i++) {
							rtac[i]=0;
							rsfrtac[i]=0;				
						}
						bp2=0.;
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0.);
					} else {
						bp2=loganREF(REFtac, rtac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						bpRSF1=loganREF(REFrsftac, rsfrtac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						SUVR1=SUVR(REFtac, rtac, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfrtac, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, bp2, bpRSF2, SUVR2, SUVRRSF2);
					}
				}
				if (nv!=0){
					for (i=0; i<nframes; i++)
					{
						tac[i]=(ltac[i]*nv1+rtac[i]*nv2)/(nv);
						rsftac[i]=(rsfltac[i]*nv1+rsfrtac[i]*nv2)/(nv);
					}
					bp=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
					bpRSF=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
					suvr=SUVR(REFtac, tac, frd, nframes, stframe, endframe);
					SUVRRSF=SUVR(REFrsftac, rsftac, frd, nframes, stframe, endframe);
					fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, bpRSF, suvr, SUVRRSF);
				} else {
					fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", 0., 0., 0., 0., 0., 0.);
				}
				break;
				
			case 2:
				sprintf(fn1, "%s_ctx-lh-%s_RSF.tac",PID,brois[i]);
				sprintf(fn2, "%s_ctx-rh-%s_RSF.tac",PID,brois[i]);
				sprintf(r1, "ctx-lh-%s", brois[i]);
				sprintf(r2, "ctx-rh-%s", brois[i]);
				sprintf(r, "ctx-%s", brois[i]);
				
				if (file_exists(fn1))
					readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
				else
					nv1=0;
	
				if (file_exists(fn2))
					readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
				else
					nv2=0;
	
				if (nv1+nv2==0) {
					nv=0;
				} else {
					if (nv1==0) {
						for (i=0; i<nframes; i++) {
							ltac[i]=0;
							rsfltac[i]=0;				
						}
						bp1=0.;
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0.);
					} else {
						bp1=loganREF(REFtac, ltac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						bpRSF1=loganREF(REFrsftac, rsfltac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						SUVR1=SUVR(REFtac, ltac, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfltac, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, bp1, bpRSF1, SUVR1, SUVRRSF1);
					}
					if (nv2==0) {
						for (i=0; i<nframes; i++) {
							rtac[i]=0;
							rsfrtac[i]=0;				
						}
						bp2=0.;
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0.);
					} else {
						bp2=loganREF(REFtac, rtac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						bpRSF1=loganREF(REFrsftac, rsfrtac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						SUVR1=SUVR(REFtac, rtac, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfrtac, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, bp2, bpRSF2, SUVR2, SUVRRSF2);
					}
				}
				if (nv!=0){
					for (i=0; i<nframes; i++)
					{
						tac[i]=(ltac[i]*nv1+rtac[i]*nv2)/(nv);
						rsftac[i]=(rsfltac[i]*nv1+rsfrtac[i]*nv2)/(nv);
					}
					bp=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
					bpRSF=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
					suvr=SUVR(REFtac, tac, frd, nframes, stframe, endframe);
					SUVRRSF=SUVR(REFrsftac, rsftac, frd, nframes, stframe, endframe);
					fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, bpRSF, suvr, SUVRRSF);
					if(strcomp(brois[i],"precuneus")==0) {bpPREC=bp; bpRSFPREC=bpRSF; suvrPREC=suvr; SUVRRSFPREC=SUVRRSF;}
				} else {
					fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", 0., 0., 0., 0., 0., 0.);
					if(strcomp(brois[i],"precuneus")==0) {bpPREC=0.; bpRSFPREC=0.;suvrPREC=0.; SUVRRSFPREC=0.;}
				}
				break;
				
			case 0:
				sprintf(fn1, "%s_%s_RSF.tac",PID,brois[i]);
				sprintf(r, "%s", brois[i]);
				
				if (file_exists(fn1))
					readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
				else
					nv1=0;		
				if (nv1!=0){
					bp=loganREF(REFtac, ltac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
					bpRSF=loganREF(REFrsftac, rsfltac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
					suvr=SUVR(REFtac, ltac, frd, nframes, stframe, endframe);
					SUVRRSF=SUVR(REFrsftac, rsfltac, frd, nframes, stframe, endframe);
					fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r, nv1, bp, bpRSF, suvr, SUVRRSF);
					fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r, nv1, bp, bpRSF, suvr, SUVRRSF);
				} else {
					fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", 0., 0., 0., 0., 0., 0.);
					fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", 0., 0., 0., 0., 0., 0.);
				}
				break;
		}
	}
	
	for (i=18; i<53; i++)
	{
		switch(rflag[i]){
			case 2:
				sprintf(fn1, "%s_wm-lh-%s_RSF.tac",PID,brois[i]);
				sprintf(fn2, "%s_wm-rh-%s_RSF.tac",PID,brois[i]);
				sprintf(r1, "wm-lh-%s", brois[i]);
				sprintf(r2, "wm-rh-%s", brois[i]);
				sprintf(r, "wm-%s", brois[i]);
				
				if (file_exists(fn1))
					readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
				else
					nv1=0;
	
				if (file_exists(fn2))
					readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
				else
					nv2=0;
	
				if (nv1+nv2==0) {
					nv=0;
				} else {
					if (nv1==0) {
						for (i=0; i<nframes; i++) {
							ltac[i]=0;
							rsfltac[i]=0;				
						}
						bp1=0.;
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0.);
					} else {
						bp1=loganREF(REFtac, ltac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						bpRSF1=loganREF(REFrsftac, rsfltac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						SUVR1=SUVR(REFtac, ltac, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfltac, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, bp1, bpRSF1, SUVR1, SUVRRSF1);
					}
					if (nv2==0) {
						for (i=0; i<nframes; i++) {
							rtac[i]=0;
							rsfrtac[i]=0;				
						}
						bp2=0.;
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0.);
					} else {
						bp2=loganREF(REFtac, rtac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						bpRSF1=loganREF(REFrsftac, rsfrtac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
						SUVR1=SUVR(REFtac, rtac, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfrtac, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, bp2, bpRSF2, SUVR2, SUVRRSF2);
					}
				}
				if (nv!=0){
					for (i=0; i<nframes; i++)
					{
						tac[i]=(ltac[i]*nv1+rtac[i]*nv2)/(nv);
						rsftac[i]=(rsfltac[i]*nv1+rsfrtac[i]*nv2)/(nv);
					}
					bp=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
					bpRSF=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
					suvr=SUVR(REFtac, tac, frd, nframes, stframe, endframe);
					SUVRRSF=SUVR(REFrsftac, rsftac, frd, nframes, stframe, endframe);
					fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, bpRSF, suvr, SUVRRSF);
				} else {
					fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", 0., 0., 0., 0., 0., 0.);
				}
				break;
				
		}
	}
	
	bpPREF=0.; bpRSFPREF=0.;suvrPREF=0.; SUVRRSFPREF=0.;
	bpGR=0.; bpRSFGR=0.;suvrGR=0.; SUVRRSFGR=0.;
	bpTEMP=0.; bpRSFTEMP=0.;suvrTEMP=0.; SUVRRSFTEMP=0.;

	
	/* GR_FS */
	sprintf(fn1, "%s_ctx-lh-lateralorbitofrontal_RSF.tac",PID);
	sprintf(fn2, "%s_ctx-rh-lateralorbitofrontal_RSF.tac",PID);
	sprintf(fn3, "%s_ctx-lh-medialorbitofrontal_RSF.tac",PID);
	sprintf(fn4, "%s_ctx-rh-medialorbitofrontal_RSF.tac",PID);

	if (file_exists(fn1))
		readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0;
	
	if (file_exists(fn2))
		readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0;
		
	if (file_exists(fn3))
		readPIBtac(fn3, tac3, rsftac3, frd, st, &nv1, &nframes); 
	else
		nv3=0;
	
	if (file_exists(fn4))
		readPIBtac(fn2, tac4, rsftac4, frd, st, &nv2, &nframes);
	else
		nv4=0;
	
	nv=nv1+nv2+nv3+nv4;
	if (nv>0) {
		for (i=0; i<nframes; i++) {
			tac[i]=0;
			if (nv1>0) { tac[i]+=ltac[i]*nv1; rsftac[i]+=rsfltac[i]*nv1;}
			if (nv2>0) { tac[i]+=rtac[i]*nv2; rsftac[i]+=rsfrtac[i]*nv2;}
			if (nv3>0) { tac[i]+=tac3[i]*nv3; rsftac[i]+=rsftac3[i]*nv3;}
			if (nv4>0) { tac[i]+=tac4[i]*nv4; rsftac[i]+=rsftac4[i]*nv4;}
			tac[i]=tac[i]/nv;
			rsftac[i]=rsftac[i]/nv;		
		}
		bpGR=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
		bpRSFGR=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
		suvrGR=SUVR(REFtac, tac, frd, nframes, stframe, endframe);
		SUVRRSFGR=SUVR(REFrsftac, rsftac, frd, nframes, stframe, endframe);
		fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", "GR_FS", nv, bpGR, bpRSFGR, suvrGR, SUVRRSFGR);
	} else {
		fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", 0., 0., 0., 0., 0., 0.);
	}
	
	/* TEMP_FS */
	sprintf(fn1, "%s_ctx-lh-middletemporal_RSF.tac",PID);
	sprintf(fn2, "%s_ctx-rh-middletemporal_RSF.tac",PID);
	sprintf(fn3, "%s_ctx-lh-superiortemporal_RSF.tac",PID);
	sprintf(fn4, "%s_ctx-rh-superiortemporal_RSF.tac",PID);

	if (file_exists(fn1))
		readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0;
	
	if (file_exists(fn2))
		readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0;
		
	if (file_exists(fn3))
		readPIBtac(fn3, tac3, rsftac3, frd, st, &nv1, &nframes); 
	else
		nv3=0;
	
	if (file_exists(fn4))
		readPIBtac(fn2, tac4, rsftac4, frd, st, &nv2, &nframes);
	else
		nv4=0;
	
	nv=nv1+nv2+nv3+nv4;
	if (nv>0) {
		for (i=0; i<nframes; i++) {
			tac[i]=0;
			if (nv1>0) { tac[i]+=ltac[i]*nv1; rsftac[i]+=rsfltac[i]*nv1;}
			if (nv2>0) { tac[i]+=rtac[i]*nv2; rsftac[i]+=rsfrtac[i]*nv2;}
			if (nv3>0) { tac[i]+=tac3[i]*nv3; rsftac[i]+=rsftac3[i]*nv3;}
			if (nv4>0) { tac[i]+=tac4[i]*nv4; rsftac[i]+=rsftac4[i]*nv4;}
			tac[i]=tac[i]/nv;
			rsftac[i]=rsftac[i]/nv;		
		}
		bpTEMP=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
		bpRSFTEMP=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &intercept, &slope, &Rsq);
		suvrTEMP=SUVR(REFtac, tac, frd, nframes, stframe, endframe);
		SUVRRSFTEMP=SUVR(REFrsftac, rsftac, frd, nframes, stframe, endframe);
		fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", "TEMP_FS", nv, bpTEMP, bpRSFTEMP, suvrTEMP, SUVRRSFTEMP);
	} else {
		fprintf(fp1,"%-35s %11d %11.4f %11.4f %11.4f %11.4f\n", 0., 0., 0., 0., 0., 0.);
	}

	
	/* OCC_FS */
	sprintf(fn1, "%s_ctx-lh-cuneus_RSF.tac",PID);
	sprintf(fn2, "%s_ctx-rh-cuneus_RSF.tac",PID);
	sprintf(fn3, "%s_ctx-lh-lingual_RSF.tac",PID);
	sprintf(fn4, "%s_ctx-rh-lingual_RSF.tac",PID);

	if (file_exists(fn1))
		readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0;
	
	if (file_ex