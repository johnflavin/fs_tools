#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main(int argc, char **argv)
{
	IMAGE_4dfp *fdgpet=NULL, *mask=NULL, *patlakout=NULL;
	FILE *fp;
	float t[MAXL], frd[MAXL], AIF[MAXL];
	int j;
	char line[512]; 
	clock_t start, end;
	
	/* Read dynamic FDG PET,and mask*/
	fdgpet=read_image_4dfp(argv[1], fdgpet);
	mask=read_image_4dfp(argv[2],mask);
	
	/* Read AIF*/
	fp = fopen(argv[3],"r");
	if (!fp) errr("patlaktest", argv[3]);
	j=0;
	while (fgets(line, 512, fp)){
		sscanf(line, "%f%f%f", t+j, frd+j, AIF+j);
		j++;
	}
	start=clock();
	
	patlakout=patlakvol(fdgpet, mask, t, AIF, frd, j, 45, patlakout);
	end=clock();
	
	write_image_4dfp(patlakout->ifh.name_of_data_file, patlakout);
	printf("CPU TIME USED IS %f seconds.\n", ((double) (end - start)) / CLOCKS_PER_SEC);
}
