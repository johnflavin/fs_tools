#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

bool file_exists(const char * filename)
{
	FILE *file;
	if(file = fopen(filename, "r")) {fclose(file);        return true;   }  return false;
}
    
float SUVR(float *REF, float *ROI, float *dcf, float *frd, int nframes, int stframe, int endframe)
{
	int i;
	float SUVR, vREF, vROI;
	
	vREF=0;
	vROI=0;
	for (i=stframe-1; i<endframe; i++) {
		vREF+=REF[i]*frd[i]/dcf[i];
		vROI+=ROI[i]*frd[i]/dcf[i];
	}
	SUVR=vROI/vREF;
	return(SUVR);
}

void readPIBtac(char *fn, float *tac, float *rsftac, float *frd, float *st, float *nv, int *nframes)
{
	FILE *fp;
	char line[MAXL], dummy[MAXL];
	float fdum, *fptr1, *fptr2, *fptr3, *fptr4;
	int i;
	
	if (!(fp=fopen(fn, "r"))) errr("ROIPIB","ROIs");
	fgets(line, 512, fp);
	sscanf(line, "%s%s%s%s%s%s%f", dummy, dummy, dummy, dummy, dummy, dummy, nv);
	fptr3=tac;
	fptr4=rsftac;
	fptr2=frd;
	fptr1=st;
	*nframes=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f", &fdum, fptr1, fptr2, fptr3, fptr4);
		fptr1++;
		fptr2++;
		fptr3++;
		fptr4++;
		(*nframes)++;
	}
	fclose(fp);
}

int main(int argc, char **argv)
{
	char PID[MAXL], ROIs[MAXL], line[MAXL], LCCfn[MAXL], RCCfn[MAXL], f1[MAXL], f2[MAXL], f3[MAXL];
	char r1[MAXL], r2[MAXL], r[MAXL], fn1[MAXL], fn2[MAXL], fn3[MAXL], fn4[MAXL], tmpstr[MAXL];
	char **brois = NULL, **arois = NULL;
	FILE *fp, *fp1, *fp2, *fp3;
	float k2=0.16, *fptr;
	float bp1, bp2, bp, t[MAXL],  intercept, slope, Rsq, bpPREC, bpGR, bpTEMP, bpPREF, bpRSFPREC, bpRSFGR, bpRSFTEMP, bpRSFPREF;
	float ltac[MAXL], rsfltac[MAXL], frd[MAXL], st[MAXL], nv1, nv2, nv3, nv4, nv, suvr, tac3[MAXL], rsftac3[MAXL], tac4[MAXL], rsftac4[MAXL];
	float SUVR1, SUVR2, SUVRRSF1, SUVRRSF2, SUVRRSF, bpRSF1, bpRSF2, bpRSF, dcf[MAXL];
	float bpOCC, bppvcOCC, bpRSFOCC, suvrOCC, suvrpvcOCC, SUVRRSFOCC, bppvcPREC, bppvcGR, bppvcTEMP, bppvcPREF;
	float suvrPREC, suvrGR, suvrTEMP, suvrPREF, SUVRRSFPREC, SUVRRSFGR, SUVRRSFTEMP, SUVRRSFPREF, suvrpvcPREC, suvrpvcGR, suvrpvcTEMP, suvrpvcPREF;
	float MCBP, MCBPRSF, MCBPSUVR, MCBPSUVRRSF, MCBPpvc, MCBPSUVRpvc;
	float intc, rsq, pvcintc, pvcrsq, rsfintc, rsfrsq, bppvc, suvrpvc, dummy;
	float cf1, cf2, cf3, cf4, pvcltac[MAXL], pvcrtac[MAXL], pvctac[MAXL], pvctac3[MAXL], pvctac4[MAXL], REFpvctac[MAXL];
	float rtac[MAXL], rsfrtac[MAXL], REFtac[MAXL], REFrsftac[MAXL], tac[MAXL], rsftac[MAXL], pvc2c[MAXL];
	int i, j, rflag[MAXL], nbr, nar, stframe, endframe, nframes, intval;
	
	
	strcpy(PID, argv[1]);
	strcpy(ROIs, argv[2]);
	printf("%s\n", PID);
	
	brois = (char **)malloc((size_t)((256)*sizeof(char *)));
	if (!(fp=fopen(ROIs, "r"))) errr("ROIPIB","ROIs");
	i=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%s%d", tmpstr, &intval);
		brois[i]=(char *)malloc((size_t)(MAXL*sizeof(char)));
		if (!brois[i]) errm("ROIPIB");
		strcpy(brois[i], tmpstr);
		rflag[i]=intval;
		i++;
	}
	nbr=i;
	fclose(fp);
	/* Read info file for decay correction factor dcf */
	printf("Read .info file for decay correction factor.\n");
	sprintf(fn1, "%s.info", PID);
	if (!(fp=fopen(fn1, "r"))) errr("ROIPIB",fn1);
	nframes=0;
	fptr=dcf;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f", &dummy, &dummy, &dummy, fptr, &dummy);
		fptr++;
		(nframes)++;
	}
	fclose(fp);
	
	
	/* Read Cerebellum TACs */
	printf("Read Cerebellum TACs\n");
	sprintf(LCCfn, "%s_Left-Cerebellum-Cortex_RSF.tac", PID);
	sprintf(RCCfn, "%s_Right-Cerebellum-Cortex_RSF.tac", PID);
	if (file_exists(LCCfn))
		readPIBtac(LCCfn, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0;
	
	if (file_exists(RCCfn))
		readPIBtac(RCCfn, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0;
	
	if (nv1+nv2==0) {
		printf("Cerebellum Cortex Undefined. Abort!\n");
		exit(-1);
	} else {
		if (nv1==0) {
			for (i=0; i<nframes; i++) {
				ltac[i]=0;
				rsfltac[i]=0;				
			}
		}
		if (nv2==0) {
			for (i=0; i<nframes; i++) {
				rtac[i]=0;
				rsfrtac[i]=0;				
			}
		}
	}
	
	/* Read PVC2C correction factor */
	arois = (char **)malloc((size_t)((512)*sizeof(char *)));
	if (!(fp=fopen("PVC2CFS.txt", "r"))) errr("ROIPIB","PVC2CFS.txt");
	i=0;
	fptr=pvc2c;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%s%f%d", tmpstr, fptr, &intval);
		arois[i]=(char *)malloc((size_t)(MAXL*sizeof(char)));
		if (!arois[i]) errm("ROIPIB");
		strcpy(arois[i], tmpstr);
		i++;
		fptr++;
	}
	nar=i;
	fclose(fp);
		
	/* Generate REFERENCE TAC */
	printf("Generate REF TACs\n");
	for (i=0; i<nar; i++)
	{
		if (strcmp(arois[i],"Left-Cerebellum-Cortex")==0) cf1=pvc2c[i];
		if (strcmp(arois[i],"Right-Cerebellum-Cortex")==0) cf2=pvc2c[i];		
	}
	
	for (i=0; i<nframes; i++)
	{
		REFtac[i]=(ltac[i]*nv1+rtac[i]*nv2)/(nv1+nv2);
		REFrsftac[i]=(rsfltac[i]*nv1+rsfrtac[i]*nv2)/(nv1+nv2);
		REFpvctac[i]=(ltac[i]*nv1/cf1+rtac[i]*nv2/cf2)/(nv1+nv2);
		t[i]=st[i]+frd[i]/2.-st[0];
	}
	
	/* ROI Modeling */
	printf("Start Modeling\n");
	sprintf(f1, "%s_ROIPIB.txt", PID);
	sprintf(f2, "%s_ROIPIBLR.txt",PID);
	stframe=nframes-5;
	endframe=nframes;
	if (!(fp1 =fopen(f1, "w"))) errw("ROIPIB", f1);
	if (!(fp2 =fopen(f2, "w"))) errw("ROIPIB", f2);
	fprintf(fp1, "%-35s %11s %11s %11s %11s %11s %11s %11s %11s %11s %11s %11s %11s %11s\n", "Structure_Name", "NVox", "bp", "R^2(bp)", "Intc(bp)", "bp(RSF)", "R^2(RSF)", "Intc(RSF)", "bp(PVC2C)", "R^2(PVC2C)", "Intc(PVC2C)", "SUVR", "SUVR(RSF)", "SUVR(PVC2C)");
	fprintf(fp2, "%-35s %11s %11s %11s %11s %11s %11s %11s %11s %11s %11s %11s %11s %11s\n", "Structure_Name", "NVox", "bp", "R^2(bp)", "Intc(bp)", "bp(RSF)", "R^2(RSF)", "Intc(RSF)", "bp(PVC2C)", "R^2(PVC2C)", "Intc(PVC2C)", "SUVR", "SUVR(RSF)", "SUVR(PVC2C)");
	
	for (i=0; i<54; i++)
	{
		cf1=1.;
		cf2=1.;
		printf("%s\n",brois[i]);
		switch(rflag[i]){
			case 1:
				sprintf(fn1, "%s_Left-%s_RSF.tac",PID,brois[i]);
				sprintf(fn2, "%s_Right-%s_RSF.tac",PID,brois[i]);
				sprintf(r1, "Left-%s", brois[i]);
				sprintf(r2, "Right-%s", brois[i]);
				sprintf(r, "%s", brois[i]);
				
				for (j=0; j<nar; j++)
				{
					if (strcmp(arois[j],r1)==0) cf1=pvc2c[j];
					if (strcmp(arois[j],r2)==0) cf2=pvc2c[j];		
				}

				if (file_exists(fn1))
					readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes);					 
				else
					nv1=0.;
	
				if (file_exists(fn2))
					readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
				else
					nv2=0.;
	
				nv=nv1+nv2;
				printf("%f\n",nv);
				if (nv>0.1) {
					if (nv1==0.) {
						for (j=0; j<nframes; j++) {
							ltac[j]=0.;
							rsfltac[j]=0.;
							pvcltac[j]=0.;				
						}
						bp1=0.;
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					} else {
						for (j=0; j<nframes; j++) {
							pvcltac[j]=ltac[j]/cf1;				
						}					
						bp1=loganREF(REFtac, ltac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
						bpRSF1=loganREF(REFrsftac, rsfltac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
						bppvc=loganREF(REFpvctac, pvcltac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
						SUVR1=SUVR(REFtac, ltac, dcf, frd, nframes, stframe, endframe);
						suvrpvc=SUVR(REFpvctac, pvcltac, dcf, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfltac, dcf, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, bp1, rsq, intc, bpRSF1, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,SUVR1, SUVRRSF1, suvrpvc);
					}
					if (nv2==0.) {
						for (j=0; j<nframes; j++) {
							rtac[j]=0.;
							rsfrtac[j]=0.;				
							pvcltac[j]=0.;				
						}
						bp2=0.;
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					} else {
						for (j=0; j<nframes; j++) {
							pvcrtac[j]=rtac[j]/cf2;				
						}					
						bp2=loganREF(REFtac, rtac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
						bpRSF2=loganREF(REFrsftac, rsfrtac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
						bppvc=loganREF(REFpvctac, pvcrtac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
						SUVR2=SUVR(REFtac, rtac, dcf, frd, nframes, stframe, endframe);
						SUVRRSF2=SUVR(REFrsftac, rsfrtac, dcf, frd, nframes, stframe, endframe);
						suvrpvc=SUVR(REFpvctac, pvcrtac, dcf, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, bp2, rsq, intc, bpRSF2, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,SUVR2, SUVRRSF2, suvrpvc);
					}
					for (j=0; j<nframes; j++)
					{
						tac[j]=(ltac[j]*nv1+rtac[j]*nv2)/(nv);
						rsftac[j]=(rsfltac[j]*nv1+rsfrtac[j]*nv2)/(nv);
						pvctac[j]=(ltac[j]*nv1/cf1+rtac[j]*nv2/cf2)/(nv);
					}
					bp=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
					bpRSF=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
					bppvc=loganREF(REFpvctac, pvctac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
					suvr=SUVR(REFtac, tac, dcf, frd, nframes, stframe, endframe);
					SUVRRSF=SUVR(REFrsftac, rsftac, dcf, frd, nframes, stframe, endframe);
					suvrpvc=SUVR(REFpvctac, pvctac, dcf, frd, nframes, stframe, endframe);
					fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
				} else {
					fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
				}
					printf("%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
				break;
				
			case 2:
				sprintf(fn1, "%s_ctx-lh-%s_RSF.tac",PID,brois[i]);
				sprintf(fn2, "%s_ctx-rh-%s_RSF.tac",PID,brois[i]);
				sprintf(r1, "ctx-lh-%s", brois[i]);
				sprintf(r2, "ctx-rh-%s", brois[i]);
				sprintf(r, "ctx-%s", brois[i]);
				
				for (j=0; j<nar; j++)
				{
					if (strcmp(arois[j],r1)==0) cf1=pvc2c[j];
					if (strcmp(arois[j],r2)==0) cf2=pvc2c[j];		
				}

				if (file_exists(fn1))
					readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
				else
					nv1=0.;
	
				if (file_exists(fn2))
					readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
				else
					nv2=0.;
	
				nv=nv1+nv2;
				if (nv>0.1) {
					if (nv1==0.) {
						for (j=0; j<nframes; j++) {
							ltac[j]=0.;
							rsfltac[j]=0.;				
							pvcltac[j]=0.;				
						}
						bp1=0.;
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					} else {
						for (j=0; j<nframes; j++) {
							pvcltac[j]=ltac[j]/cf1;				
						}					
						bp1=loganREF(REFtac, ltac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
						bpRSF1=loganREF(REFrsftac, rsfltac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
						bppvc=loganREF(REFpvctac, pvcltac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
						SUVR1=SUVR(REFtac, ltac, dcf, frd, nframes, stframe, endframe);
						suvrpvc=SUVR(REFpvctac, pvcltac, dcf, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfltac, dcf, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, bp1, rsq, intc, bpRSF1, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,SUVR1, SUVRRSF1, suvrpvc);
					}
					if (nv2==0.) {
						for (j=0; j<nframes; j++) {
							rtac[j]=0.;
							rsfrtac[j]=0.;				
							pvcltac[j]=0.;				
						}
						bp2=0.;
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					} else {
						for (j=0; j<nframes; j++) {
							pvcrtac[j]=rtac[j]/cf2;				
						}					
						bp2=loganREF(REFtac, rtac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
						bpRSF2=loganREF(REFrsftac, rsfrtac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
						bppvc=loganREF(REFpvctac, pvcrtac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
						SUVR2=SUVR(REFtac, rtac, dcf, frd, nframes, stframe, endframe);
						SUVRRSF2=SUVR(REFrsftac, rsfrtac, dcf, frd, nframes, stframe, endframe);
						suvrpvc=SUVR(REFpvctac, pvcrtac, dcf, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, bp2, rsq, intc, bpRSF2, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,SUVR2, SUVRRSF2, suvrpvc);
					}
					for (j=0; j<nframes; j++)
					{
						tac[j]=(ltac[j]*nv1+rtac[j]*nv2)/(nv);
						rsftac[j]=(rsfltac[j]*nv1+rsfrtac[j]*nv2)/(nv);
						pvctac[j]=(ltac[j]*nv1/cf1+rtac[j]*nv2/cf2)/(nv);
					}
					bp=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
					bpRSF=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
					bppvc=loganREF(REFpvctac, pvctac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
					suvr=SUVR(REFtac, tac, dcf, frd, nframes, stframe, endframe);
					SUVRRSF=SUVR(REFrsftac, rsftac, dcf, frd, nframes, stframe, endframe);
					suvrpvc=SUVR(REFpvctac, pvctac, dcf, frd, nframes, stframe, endframe);
					fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
					if(strcmp(brois[i],"precuneus")==0) {bpPREC=bp; bppvcPREC=bppvc, bpRSFPREC=bpRSF; suvrPREC=suvr; suvrpvcPREC=suvrpvc; SUVRRSFPREC=SUVRRSF;}
				} else {
					fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					if(strcmp(brois[i],"precuneus")==0) {bpPREC=0.; bpRSFPREC=0.; bppvcPREC=0.; suvrPREC=0.; suvrpvcPREC=0.; SUVRRSFPREC=0.;}
				}
					printf("%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
				break;
				
			case 0:
				sprintf(fn1, "%s_%s_RSF.tac",PID,brois[i]);
				sprintf(r, "%s", brois[i]);
				for (j=0; j<nar; j++)
				{
					if (strcmp(arois[j],r)==0) cf1=pvc2c[j];
				}
				
				if (file_exists(fn1))
					readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
				else
					nv1=0.;		
				if (nv1>0.){
					for (j=0; j<nframes; j++) {
						pvcltac[j]=ltac[j]/cf1;				
					}					
					bp=loganREF(REFtac, ltac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
					bpRSF=loganREF(REFrsftac, rsfltac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
					bppvc=loganREF(REFpvctac, pvcltac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
					suvr=SUVR(REFtac, ltac, dcf, frd, nframes, stframe, endframe);
					SUVRRSF=SUVR(REFrsftac, rsfltac, dcf, frd, nframes, stframe, endframe);
					suvrpvc=SUVR(REFpvctac, pvcltac, dcf, frd, nframes, stframe, endframe);
					fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv1, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
					fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv1, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
				} else {
					fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
				}
					printf("%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv1, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
				break;
		}
	}
	
	for (i=18; i<53; i++)
	{
		switch(rflag[i]){
			case 2:
				sprintf(fn1, "%s_wm-lh-%s_RSF.tac",PID,brois[i]);
				sprintf(fn2, "%s_wm-rh-%s_RSF.tac",PID,brois[i]);
				sprintf(r1, "wm-lh-%s", brois[i]);
				sprintf(r2, "wm-rh-%s", brois[i]);
				sprintf(r, "wm-%s", brois[i]);
				
				for (j=0; j<nar; j++)
				{
					if (strcmp(arois[j],r1)==0) cf1=pvc2c[j];
					if (strcmp(arois[j],r2)==0) cf2=pvc2c[j];		
				}

				if (file_exists(fn1))
					readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
				else
					nv1=0.;
	
				if (file_exists(fn2))
					readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
				else
					nv2=0.;
	
				nv=nv1+nv2;
				if (nv>0.1) {
					if (nv1==0.) {
						for (j=0; j<nframes; j++) {
							ltac[j]=0.;
							rsfltac[j]=0.;				
							pvcltac[j]=0.;				
						}
						bp1=0.;
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					} else {
						for (j=0; j<nframes; j++) {
							pvcltac[j]=ltac[j]/cf1;				
						}					
						bp1=loganREF(REFtac, ltac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
						bpRSF1=loganREF(REFrsftac, rsfltac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
						bppvc=loganREF(REFpvctac, pvcltac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
						SUVR1=SUVR(REFtac, ltac, dcf, frd, nframes, stframe, endframe);
						suvrpvc=SUVR(REFpvctac, pvcltac, dcf, frd, nframes, stframe, endframe);
						SUVRRSF1=SUVR(REFrsftac, rsfltac, dcf, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, bp1, rsq, intc, bpRSF1, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,SUVR1, SUVRRSF1, suvrpvc);
					}
					if (nv2==0.) {
						for (j=0; j<nframes; j++) {
							rtac[j]=0.;
							rsfrtac[j]=0.;				
							pvcltac[j]=0.;				
						}
						bp2=0.;
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					} else {
						for (j=0; j<nframes; j++) {
							pvcrtac[j]=rtac[j]/cf2;				
						}					
						bp2=loganREF(REFtac, rtac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
						bpRSF2=loganREF(REFrsftac, rsfrtac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
						bppvc=loganREF(REFpvctac, pvcrtac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
						SUVR2=SUVR(REFtac, rtac, dcf, frd, nframes, stframe, endframe);
						SUVRRSF2=SUVR(REFrsftac, rsfrtac, dcf, frd, nframes, stframe, endframe);
						suvrpvc=SUVR(REFpvctac, pvcrtac, dcf, frd, nframes, stframe, endframe);
						fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, bp2, rsq, intc, bpRSF2, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,SUVR2, SUVRRSF2, suvrpvc);
					}
					for (j=0; j<nframes; j++)
					{
						tac[j]=(ltac[j]*nv1+rtac[j]*nv2)/(nv);
						rsftac[j]=(rsfltac[j]*nv1+rsfrtac[j]*nv2)/(nv);
						pvctac[j]=(ltac[j]*nv1/cf1+rtac[j]*nv2/cf2)/(nv);
					}
					bp=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
					bpRSF=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
					bppvc=loganREF(REFpvctac, pvctac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
					suvr=SUVR(REFtac, tac, dcf, frd, nframes, stframe, endframe);
					SUVRRSF=SUVR(REFrsftac, rsftac, dcf, frd, nframes, stframe, endframe);
					suvrpvc=SUVR(REFpvctac, pvctac, dcf, frd, nframes, stframe, endframe);
					fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
				} else {
					fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r1, nv1, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					fprintf(fp2,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r2, nv2, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
					fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
				}
					printf("%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", r, nv, bp, rsq, intc, bpRSF, rsfrsq, rsfintc, bppvc, pvcrsq, pvcintc,suvr, SUVRRSF, suvrpvc);
				break;
			
		}
	}
	
	bpPREF=0.; bpRSFPREF=0.;suvrPREF=0.; SUVRRSFPREF=0.; bppvcPREF=0.; suvrpvcPREF=0;
	bpGR=0.; bpRSFGR=0.;suvrGR=0.; SUVRRSFGR=0.; bppvcGR=0.; suvrpvcGR=0;
	bpTEMP=0.; bpRSFTEMP=0.;suvrTEMP=0.; SUVRRSFTEMP=0.;bppvcTEMP=0.; suvrpvcTEMP=0;

	
	/* GR_FS */
	sprintf(fn1, "%s_ctx-lh-lateralorbitofrontal_RSF.tac",PID);
	sprintf(fn2, "%s_ctx-rh-lateralorbitofrontal_RSF.tac",PID);
	sprintf(fn3, "%s_ctx-lh-medialorbitofrontal_RSF.tac",PID);
	sprintf(fn4, "%s_ctx-rh-medialorbitofrontal_RSF.tac",PID);
	cf1=1.; cf2=1.; cf3=1.; cf4=1.;
	for (j=0; j<nar; j++)
	{
		if (strcmp(arois[j],"ctx-lh-lateralorbitofrontal")==0) cf1=pvc2c[j];
		if (strcmp(arois[j],"ctx-rh-lateralorbitofrontal")==0) cf2=pvc2c[j];		
		if (strcmp(arois[j],"ctx-lh-medialorbitofrontal")==0) cf3=pvc2c[j];
		if (strcmp(arois[j],"ctx-rh-medialorbitofrontal")==0) cf4=pvc2c[j];		
	}

	if (file_exists(fn1))
		readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0.;
	
	if (file_exists(fn2))
		readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0.;
		
	if (file_exists(fn3))
		readPIBtac(fn3, tac3, rsftac3, frd, st, &nv3, &nframes); 
	else
		nv3=0.;
	
	if (file_exists(fn4))
		readPIBtac(fn4, tac4, rsftac4, frd, st, &nv4, &nframes);
	else
		nv4=0.;
	
	nv=nv1+nv2+nv3+nv4;
	if (nv>0.) {
		for (i=0; i<nframes; i++) {
			tac[i]=0.;
			rsftac[i]=0.;
			pvctac[i]=0.;
			if (nv1>0.) { tac[i]+=ltac[i]*nv1; rsftac[i]+=rsfltac[i]*nv1; pvctac[i]+=ltac[i]*nv1/cf1;}
			if (nv2>0.) { tac[i]+=rtac[i]*nv2; rsftac[i]+=rsfrtac[i]*nv2; pvctac[i]+=ltac[i]*nv2/cf2;}
			if (nv3>0.) { tac[i]+=tac3[i]*nv3; rsftac[i]+=rsftac3[i]*nv3; pvctac[i]+=ltac[i]*nv3/cf3;}
			if (nv4>0.) { tac[i]+=tac4[i]*nv4; rsftac[i]+=rsftac4[i]*nv4; pvctac[i]+=ltac[i]*nv4/cf4;}
			tac[i]=tac[i]/nv;
			rsftac[i]=rsftac[i]/nv;		
			pvctac[i]=pvctac[i]/nv;		
		}
		bpGR=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
		bpRSFGR=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
		bppvcGR=loganREF(REFpvctac, pvctac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
		suvrGR=SUVR(REFtac, tac, dcf, frd, nframes, stframe, endframe);
		SUVRRSFGR=SUVR(REFrsftac, rsftac, dcf, frd, nframes, stframe, endframe);
		suvrpvcGR=SUVR(REFpvctac, pvctac, dcf, frd, nframes, stframe, endframe);
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "GR_FS", nv, bpGR, rsq, intc, bpRSFGR, rsfrsq, rsfintc, bppvcGR, pvcrsq, pvcintc, suvrGR, SUVRRSFGR, suvrpvcGR);
	} else {
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "GR_FS", nv, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
	}
	
	/* TEMP_FS */
	sprintf(fn1, "%s_ctx-lh-middletemporal_RSF.tac",PID);
	sprintf(fn2, "%s_ctx-rh-middletemporal_RSF.tac",PID);
	sprintf(fn3, "%s_ctx-lh-superiortemporal_RSF.tac",PID);
	sprintf(fn4, "%s_ctx-rh-superiortemporal_RSF.tac",PID);
	cf1=1.; cf2=1.; cf3=1.; cf4=1.;
	for (j=0; j<nar; j++)
	{
		if (strcmp(arois[j],"ctx-lh-middletemporal")==0) cf1=pvc2c[j];
		if (strcmp(arois[j],"ctx-rh-middletemporal")==0) cf2=pvc2c[j];		
		if (strcmp(arois[j],"ctx-lh-superiortemporal")==0) cf3=pvc2c[j];
		if (strcmp(arois[j],"ctx-rh-superiortemporal")==0) cf4=pvc2c[j];		
	}

	if (file_exists(fn1))
		readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0.;
	
	if (file_exists(fn2))
		readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0.;
		
	if (file_exists(fn3))
		readPIBtac(fn3, tac3, rsftac3, frd, st, &nv3, &nframes); 
	else
		nv3=0.;
	
	if (file_exists(fn4))
		readPIBtac(fn4, tac4, rsftac4, frd, st, &nv4, &nframes);
	else
		nv4=0.;
	
	nv=nv1+nv2+nv3+nv4;
	if (nv>0.) {
		for (i=0; i<nframes; i++) {
			tac[i]=0.;
			rsftac[i]=0.;
			pvctac[i]=0.;
			if (nv1>0.) { tac[i]+=ltac[i]*nv1; rsftac[i]+=rsfltac[i]*nv1; pvctac[i]+=ltac[i]*nv1/cf1;}
			if (nv2>0.) { tac[i]+=rtac[i]*nv2; rsftac[i]+=rsfrtac[i]*nv2; pvctac[i]+=ltac[i]*nv2/cf2;}
			if (nv3>0.) { tac[i]+=tac3[i]*nv3; rsftac[i]+=rsftac3[i]*nv3; pvctac[i]+=ltac[i]*nv3/cf3;}
			if (nv4>0.) { tac[i]+=tac4[i]*nv4; rsftac[i]+=rsftac4[i]*nv4; pvctac[i]+=ltac[i]*nv4/cf4;}
			tac[i]=tac[i]/nv;
			rsftac[i]=rsftac[i]/nv;		
			pvctac[i]=pvctac[i]/nv;		
		}
		bpTEMP=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
		bpRSFTEMP=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
		bppvcTEMP=loganREF(REFpvctac, pvctac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
		suvrTEMP=SUVR(REFtac, tac, dcf, frd, nframes, stframe, endframe);
		SUVRRSFTEMP=SUVR(REFrsftac, rsftac, dcf, frd, nframes, stframe, endframe);
		suvrpvcTEMP=SUVR(REFpvctac, pvctac, dcf, frd, nframes, stframe, endframe);
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "TEMP_FS", nv, bpTEMP, rsq, intc, bpRSFTEMP, rsfrsq, rsfintc, bppvcTEMP, pvcrsq, pvcintc, suvrTEMP, SUVRRSFTEMP, suvrpvcTEMP);
	} else {
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "TEMP_FS", nv, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
	}


	
	/* OCC_FS */
	sprintf(fn1, "%s_ctx-lh-cuneus_RSF.tac",PID);
	sprintf(fn2, "%s_ctx-rh-cuneus_RSF.tac",PID);
	sprintf(fn3, "%s_ctx-lh-lingual_RSF.tac",PID);
	sprintf(fn4, "%s_ctx-rh-lingual_RSF.tac",PID);

	cf1=1.; cf2=1.; cf3=1.; cf4=1.;
	for (j=0; j<nar; j++)
	{
		if (strcmp(arois[j],"ctx-lh-cuneus")==0) cf1=pvc2c[j];
		if (strcmp(arois[j],"ctx-rh-cuneus")==0) cf2=pvc2c[j];		
		if (strcmp(arois[j],"ctx-lh-lingual")==0) cf3=pvc2c[j];
		if (strcmp(arois[j],"ctx-rh-lingual")==0) cf4=pvc2c[j];		
	}

	if (file_exists(fn1))
		readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0.;
	
	if (file_exists(fn2))
		readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0.;
		
	if (file_exists(fn3))
		readPIBtac(fn3, tac3, rsftac3, frd, st, &nv3, &nframes); 
	else
		nv3=0.;
	
	if (file_exists(fn4))
		readPIBtac(fn4, tac4, rsftac4, frd, st, &nv4, &nframes);
	else
		nv4=0.;
	
	nv=nv1+nv2+nv3+nv4;
	if (nv>0.) {
		for (i=0; i<nframes; i++) {
			tac[i]=0.;
			rsftac[i]=0.;
			pvctac[i]=0.;
			if (nv1>0.) { tac[i]+=ltac[i]*nv1; rsftac[i]+=rsfltac[i]*nv1; pvctac[i]+=ltac[i]*nv1/cf1;}
			if (nv2>0.) { tac[i]+=rtac[i]*nv2; rsftac[i]+=rsfrtac[i]*nv2; pvctac[i]+=ltac[i]*nv2/cf2;}
			if (nv3>0.) { tac[i]+=tac3[i]*nv3; rsftac[i]+=rsftac3[i]*nv3; pvctac[i]+=ltac[i]*nv3/cf3;}
			if (nv4>0.) { tac[i]+=tac4[i]*nv4; rsftac[i]+=rsftac4[i]*nv4; pvctac[i]+=ltac[i]*nv4/cf4;}
			tac[i]=tac[i]/nv;
			rsftac[i]=rsftac[i]/nv;		
			pvctac[i]=pvctac[i]/nv;		
		}
		bpOCC=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
		bpRSFOCC=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
		bppvcOCC=loganREF(REFpvctac, pvctac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
		suvrOCC=SUVR(REFtac, tac, dcf, frd, nframes, stframe, endframe);
		SUVRRSFOCC=SUVR(REFrsftac, rsftac, dcf, frd, nframes, stframe, endframe);
		suvrpvcOCC=SUVR(REFpvctac, pvctac, dcf, frd, nframes, stframe, endframe);
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "OCC_FS", nv, bpOCC, rsq, intc, bpRSFOCC, rsfrsq, rsfintc, bppvcOCC, pvcrsq, pvcintc, suvrOCC, SUVRRSFOCC, suvrpvcOCC);
	} else {
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "OCC_FS", nv, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
	}

	
	/* PREF_FS */
	sprintf(fn1, "%s_ctx-lh-rostralmiddlefrontal_RSF.tac",PID);
	sprintf(fn2, "%s_ctx-rh-rostralmiddlefrontal_RSF.tac",PID);
	sprintf(fn3, "%s_ctx-lh-superiorfrontal_RSF.tac",PID);
	sprintf(fn4, "%s_ctx-rh-superiorfrontal_RSF.tac",PID);

	cf1=1.; cf2=1.; cf3=1.; cf4=1.;
	for (j=0; j<nar; j++)
	{
		if (strcmp(arois[j],"ctx-lh-rostralmiddlefrontal")==0) cf1=pvc2c[j];
		if (strcmp(arois[j],"ctx-rh-rostralmiddlefrontal")==0) cf2=pvc2c[j];		
		if (strcmp(arois[j],"ctx-lh-superiorfrontal")==0) cf3=pvc2c[j];
		if (strcmp(arois[j],"ctx-rh-superiorfrontal")==0) cf4=pvc2c[j];		
	}

	if (file_exists(fn1))
		readPIBtac(fn1, ltac, rsfltac, frd, st, &nv1, &nframes); 
	else
		nv1=0.;
	
	if (file_exists(fn2))
		readPIBtac(fn2, rtac, rsfrtac, frd, st, &nv2, &nframes);
	else
		nv2=0.;
		
	if (file_exists(fn3))
		readPIBtac(fn3, tac3, rsftac3, frd, st, &nv3, &nframes); 
	else
		nv3=0.;
	
	if (file_exists(fn4))
		readPIBtac(fn4, tac4, rsftac4, frd, st, &nv4, &nframes);
	else
		nv4=0.;
	
	nv=nv1+nv2+nv3+nv4;
	if (nv>0.) {
		for (i=0; i<nframes; i++) {
			tac[i]=0.;
			rsftac[i]=0.;
			pvctac[i]=0.;
			if (nv1>0.) { tac[i]+=ltac[i]*nv1; rsftac[i]+=rsfltac[i]*nv1; pvctac[i]+=ltac[i]*nv1/cf1;}
			if (nv2>0.) { tac[i]+=rtac[i]*nv2; rsftac[i]+=rsfrtac[i]*nv2; pvctac[i]+=ltac[i]*nv2/cf2;}
			if (nv3>0.) { tac[i]+=tac3[i]*nv3; rsftac[i]+=rsftac3[i]*nv3; pvctac[i]+=ltac[i]*nv3/cf3;}
			if (nv4>0.) { tac[i]+=tac4[i]*nv4; rsftac[i]+=rsftac4[i]*nv4; pvctac[i]+=ltac[i]*nv4/cf4;}
			tac[i]=tac[i]/nv;
			rsftac[i]=rsftac[i]/nv;		
			pvctac[i]=pvctac[i]/nv;		
		}
		bpPREF=loganREF(REFtac, tac, k2, t,  frd, nframes, stframe, endframe, &intc, &slope, &rsq);
		bpRSFPREF=loganREF(REFrsftac, rsftac, k2, t,  frd, nframes, stframe, endframe, &rsfintc, &slope, &rsfrsq);
		bppvcPREF=loganREF(REFpvctac, pvctac, k2, t,  frd, nframes, stframe, endframe, &pvcintc, &slope, &pvcrsq);
		suvrPREF=SUVR(REFtac, tac, dcf, frd, nframes, stframe, endframe);
		SUVRRSFPREF=SUVR(REFrsftac, rsftac, dcf, frd, nframes, stframe, endframe);
		suvrpvcPREF=SUVR(REFpvctac, pvctac, dcf, frd, nframes, stframe, endframe);
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "PREF_FS", nv, bpPREF, rsq, intc, bpRSFPREF, rsfrsq, rsfintc, bppvcPREF, pvcrsq, pvcintc, suvrPREF, SUVRRSFPREF, suvrpvcPREF);
	} else {
		fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "PREF_FS", nv, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.);
	}

	printf("Finished Modeling\n");
	
	/* MCBP */	
	MCBP=(bpPREC+bpGR+bpTEMP+bpPREF)/4.;
	MCBPRSF=(bpRSFPREC+bpRSFGR+bpRSFTEMP+bpRSFPREF)/4.;
	MCBPpvc=(bppvcPREC+bppvcGR+bppvcTEMP+bppvcPREF)/4.;
	MCBPSUVR=(suvrPREC+suvrGR+suvrTEMP+suvrPREF)/4.;
	MCBPSUVRpvc=(suvrpvcPREC+suvrpvcGR+suvrpvcTEMP+suvrpvcPREF)/4.;
	MCBPSUVRRSF=(SUVRRSFPREC+SUVRRSFGR+SUVRRSFTEMP+SUVRRSFPREF)/4.;	
	fprintf(fp1,"%-35s %11.0f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.9f %11.4f %11.4f %11.4f %11.4f\n", "MCBP", 0., MCBP, 0., 0., MCBPRSF, 0., 0., MCBPpvc, 0., 0., MCBPSUVR, MCBPSUVRRSF, MCBPSUVRpvc);
	
	fclose(fp1);
	fclose(fp2);
}
